<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/TimberPanels.Master" CodeBehind="ReportAndPrint.aspx.vb" Inherits="Downtime.ReportAndPrint" 
    title="Downtime: Report and Print Form" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
       Report and Print Form
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
       <div style="visibility:<%=Session("ReportLinkVisible")%>"><p><a href="<%=DataAccess.DataAccessComponent.InsightPortalPage%>" target="_blank">Downtime Report and Print Form Link &raquo;</a></p></div>
       <div style="visibility:<%=Session("RMJReportLinkVisible")%>"><p><a href="<%=DataAccess.DataAccessComponent.RMJInsightPortalPage%>" target="_blank">Mt. Gambier Downtime Report and Print Form Link &raquo;</a></p></div>
       <div style="visibility:<%=Session("WCAReportLinkVisible")%>"><p><a href="<%=DataAccess.DataAccessComponent.WCAInsightPortalPage%>" target="_blank">Caboolture Downtime Report and Print Form Link &raquo;</a></p></div>
       <div style="visibility:<%=Session("ReportLinkNoAccess")%>"><p>Sorry you do not have access to the application.</p></div>
</asp:Content>
