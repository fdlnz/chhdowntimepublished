<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/TimberPanels.Master" CodeBehind="MaintCrew.aspx.vb" Inherits="Downtime.MaintCrew" 
    title="Downtime: Maintain Crew" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
   Maintain Crew
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
  <table style="width: 100%;" class="content">
	   <tr>
			<td>
			<asp:ObjectDataSource ID="odsPlant" runat="server" OldValuesParameterFormatString="original_{0}"
             SelectMethod="GetData" TypeName="DataAccess.PlantListDataSetTableAdapters.GetALLPlantTableAdapter">
              <SelectParameters>
                 <asp:Parameter DefaultValue="Y" Name="AddBlankEntry" Type="String" />
                 <asp:Parameter DefaultValue="" Name="BlankEntryName" Type="String" />
               </SelectParameters>
               </asp:ObjectDataSource>
             </td>
		  </tr>
		  <tr>
			<td>
			<asp:ObjectDataSource ID="odsCrew" runat="server" 
                    SelectMethod="GetData" TypeName="DataAccess.CrewForMaintDataSetTableAdapters.GetALLCrewForMaintTableAdapter" 
                    OldValuesParameterFormatString="original_{0}">
                </asp:ObjectDataSource>
              </td>
          </tr>
          <tr>
          <td >
                <p><asp:Label ID="lbMessage" runat="server" CssClass="Msg"></asp:Label>
                </p>
            </td>
          </tr>
          <tr>
		    <td>&nbsp;
		      <asp:GridView ID="gvCrew" runat="server" AutoGenerateColumns="False" 
                    DataSourceID="odsCrew" AllowPaging="True" PageSize="50" ForeColor="Black">
                    <Columns>
                         <asp:TemplateField HeaderText="Hidden" InsertVisible="False" Visible="False" >
                            <ItemTemplate>
                                <asp:Label ID="lbOldCrew" runat="server" Text='<%# Bind("Crew") %>' Visible="False"></asp:Label>
                                <asp:Label ID="lbOldPlant" runat="server" Text='<%# Bind("plant_code") %>' Visible="False"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Plant" >
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                             <asp:DropDownList ID="dplPlant" runat="server"  Width="350px" DataSourceID="odsPlant" DataTextField="plant_name" DataValueField="plant_code" SelectedValue='<%# Eval("plant_code") %>' ></asp:DropDownList>                            
                            </ItemTemplate>
                        </asp:TemplateField>  
                        <asp:TemplateField HeaderText="Crew" >
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                            <asp:textBox ID="txtNewCrew" runat="server" Text='<%# Eval("Crew") %>' Width="100px"/>                            
                            </ItemTemplate>
                        </asp:TemplateField>  
                         <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button ID="btnEdit" runat="server" Text='<%# IIF(EVAL("Crew") = "","Save","Update") %>' 
                                CommandName="EditCrew" CommandArgument='<%#  CType(Container,GridViewRow).RowIndex  %>' />&nbsp;&nbsp;
                               <asp:Button ID="btnDelete" runat="server" Text="Delete"  Visible='<%# IIF(EVAL("Crew") = "","False","True") %>'
                                CommandName="DeleteCrew" CommandArgument='<%#  CType(Container,GridViewRow).RowIndex  %>'
                                OnClientClick="return confirm('Are you sure you want to delete?');"/>
                            </ItemTemplate>
                        </asp:TemplateField>    
                    </Columns>                   
                    <HeaderStyle BackColor="#669999" Font-Bold="True" ForeColor="Black" />
                    <AlternatingRowStyle BackColor="#E0E0E0" />
                </asp:GridView>
			    
		    </td>
		</tr>
		<tr >
			<td>
			<asp:button id="btnCancel" runat="server" Text="Back"></asp:button>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr style="height: 100%">
			<td ></td>
		</tr>
	</table>
</asp:Content>
