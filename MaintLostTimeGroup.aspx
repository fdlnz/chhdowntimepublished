﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/TimberPanels.Master" CodeBehind="MaintLostTimeGroup.aspx.vb" Inherits="Downtime.MaintLostTimeGroup" 
    title="Downtime: Maintain Lost Time Group" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
   Maintain Lost Time Group
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <table style="width: 100%;" class="content">
        <tr>
            <td>
                <asp:ObjectDataSource ID="odsLostTime" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetData" TypeName="DataAccess.LostTimeForMaintDataSetTableAdapters.GetLostTimeForMaintTableAdapter">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="Y" Name="AddBlankEntry" Type="String" />
                        <asp:Parameter DefaultValue="" Name="BlankEntryName" Type="String" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="LostTimeCode" Type="String" />
                        <asp:Parameter Name="LostTimeDesc" Type="String" />
                    </UpdateParameters>
                     <DeleteParameters>
                        <asp:Parameter Name="LostTimeCode" Type="String" />
                    </DeleteParameters>
                </asp:ObjectDataSource>
            </td>
        </tr>
        <tr>
            <asp:Label runat="server" ID="lbMessage" CssClass="ErrorMsgBold"></asp:Label>
        </tr>
        <tr>
		    <td>&nbsp;
		      <asp:GridView ID="gvLostTime" runat="server" AutoGenerateColumns="False"  OldValuesParameterFormatString="original_{0}" 
                    DataSourceID="odsLostTime" AllowPaging="True" PageSize="50" ForeColor="Black" DeleteMethod="Delete" UpdateMethod="Update" InsertMethod="Insert">
                    <Columns>
                        <asp:TemplateField HeaderText="Lost Time Code" InsertVisible="False"  >
                              <ItemStyle CssClass="dataGridContent" BorderStyle="None"  />
                            <ItemTemplate>
                                <asp:textBox ID="txtLostTimeCode" runat="server" Text='<%# Eval("lost_time_code")%>' Width="100px"/>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Sub Area Item Desc" >
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                            <asp:textBox ID="txtLostTimeDesc" runat="server" Text='<%# Eval("lost_time_desc")%>' Width="300px"/>                            
                            </ItemTemplate>
                        </asp:TemplateField> 
                        <asp:TemplateField>
                           <ItemStyle BorderStyle="None"  />
                            <ItemTemplate>
                                <asp:Button ID="btnEdit" runat="server" Text='<%# IIF(EVAL("lost_time_code") = "","Save","Update") %>'
                                CommandName="EditLostTime" CommandArgument='<%#  CType(Container,GridViewRow).RowIndex %>' />&nbsp;&nbsp;
                                <asp:Button ID="btnDelete" runat="server" Text="Delete" Visible='<%# IIF(EVAL("lost_time_code") = "","False","True") %>'
                                    CommandName="DeleteLostTime" CommandArgument='<%#  CType(Container,GridViewRow).RowIndex  %>'
                                    OnClientClick="return confirm('Are you sure you want to delete?');" />
                            </ItemTemplate>
                        </asp:TemplateField>    
                    </Columns>                   
                    <HeaderStyle BackColor="#669999" Font-Bold="True" ForeColor="Black" />
                    <AlternatingRowStyle BackColor="#E0E0E0" />
                </asp:GridView>
			    
		    </td>
		</tr>
    </table>
</asp:Content>

