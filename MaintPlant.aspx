<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/TimberPanels.Master" CodeBehind="MaintPlant.aspx.vb" Inherits="Downtime.MaintPlant" 
    title="Downtime: Maintain Plant" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
    Maintain Plant
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
<table style="width: 100%;" class="content">
	   <tr>
			<td>
			<asp:ObjectDataSource ID="odsPlant" runat="server" OldValuesParameterFormatString="original_{0}"
             SelectMethod="GetData" TypeName="DataAccess.PlantForMaintDataSetTableAdapters.GetALLPlantForMaintTableAdapter" UpdateMethod="Update">
                <UpdateParameters>
                    <asp:Parameter Name="PlantCode" Type="String" />
                    <asp:Parameter Name="PlantName" Type="String" />
                    <asp:Parameter Name="RequiredFlag" Type="String" />
                    <asp:Parameter Name="CommentRequiredFlag" Type="String" />
                    <asp:Parameter Name="Devision" Type="String" />
                </UpdateParameters>
               </asp:ObjectDataSource>
             </td>
		  </tr>
		  <tr>
		   <td>
                <p><asp:Label ID="lbMessage" runat="server" CssClass="Msg"></asp:Label>
                </p>
            </td>
          </tr>
          <tr>
		    <td>&nbsp;
		      <asp:GridView ID="gvPlant" runat="server" AutoGenerateColumns="False" 
                    DataSourceID="odsPlant" AllowPaging="True" PageSize="50" ForeColor="Black">
                    <Columns>
                         <asp:TemplateField HeaderText="Plant Code" InsertVisible="False"  >
                             <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                                <asp:Label ID="lbPlantCode" runat="server" Text='<%# Bind("plant_code") %>' Width="50px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                          <asp:TemplateField HeaderText="Company Code" >
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                            <asp:textBox ID="txtCompanyCode" runat="server" Text='<%# Eval("company_code") %>' Width="50px"/>                            
                            </ItemTemplate>
                        </asp:TemplateField>  
                        <asp:TemplateField HeaderText="Plant Name" >
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                            <asp:textBox ID="txtPlantName" runat="server" Text='<%# Eval("plant_name") %>' Width="350px"/>                            
                            </ItemTemplate>
                        </asp:TemplateField>  
                        <asp:TemplateField HeaderText="Required" >
                            <ItemStyle CssClass="dataGridContent" HorizontalAlign="Center" BorderStyle="None" />
                            <ItemTemplate>
                             <asp:CheckBox ID="chkRequired" runat="server" Checked='<%# Eval("required_flag") %>' ></asp:CheckBox>                            
                            </ItemTemplate>
                        </asp:TemplateField>  
                         <asp:TemplateField HeaderText="Comment Required" >
                            <ItemStyle CssClass="dataGridContent" HorizontalAlign="Center" BorderStyle="None" />
                            <ItemTemplate>
                             <asp:CheckBox ID="chkCommentRequired" runat="server" Checked='<%# Eval("comment_required_flag") %>' ></asp:CheckBox>                            
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Division" >
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                                <asp:DropDownList ID="ddlDivision" runat="server" DataValueField='' Width="68px" SelectedValue ='<%# Eval("division") %>'>
                                    <asp:ListItem Text="Panels" Value="P" ></asp:ListItem>
                                    <asp:ListItem Text="Timber" Value="T" ></asp:ListItem>
                                    <asp:ListItem Text="Legacy" Value="L" ></asp:ListItem>
                                </asp:DropDownList>                            
                            </ItemTemplate>
                        </asp:TemplateField>  
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button ID="btnEdit" runat="server" Text='Update' 
                                CommandName="EditPlant" CommandArgument='<%#  CType(Container,GridViewRow).RowIndex %>' />
                            </ItemTemplate>
                             <ItemStyle BorderStyle="None" />
                        </asp:TemplateField>    
                    </Columns>                   
                    <HeaderStyle BackColor="#669999" Font-Bold="True" ForeColor="Black" />
                    <AlternatingRowStyle BackColor="#E0E0E0" />
                </asp:GridView>
			    
		    </td>
		</tr>
		<tr >
			<td>
			<asp:button id="btnCancel" runat="server" Text="Back"></asp:button>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr style="height: 100%">
			<td ></td>
		</tr>
	</table>
</asp:Content>
