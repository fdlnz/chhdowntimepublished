<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/TimberPanels.Master" Codebehind="MaintSort.aspx.vb"
    Inherits="Downtime.MaintSort" Title="Downtime: Maintain Sort" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
    Maintain Sort
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <table style="width: 100%;" class="content">
        <tr>
            <td>
                <asp:ObjectDataSource ID="odsPlant" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetData" TypeName="DataAccess.PlantListDataSetTableAdapters.GetALLPlantTableAdapter">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="N" Name="AddBlankEntry" Type="String" />
                        <asp:Parameter DefaultValue="" Name="BlankEntryName" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
        </tr>
        <tr>
            <td>
                <asp:ObjectDataSource ID="odsSection" runat="server" SelectMethod="GetData" TypeName="DataAccess.ValidSectionListDataSetTableAdapters.GetValidSectionByPlantTableAdapter"
                    OldValuesParameterFormatString="original_{0}">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="fltPlant" Name="plant_code" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:Parameter DefaultValue="N" Name="AddBlankEntry" Type="String" />
                        <asp:Parameter DefaultValue="" Name="BlankEntryName" Type="String" />
                        <asp:SessionParameter DefaultValue="" Name="CurrentUser" SessionField="current_user"
                            Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
        </tr>
        <tr>
            <td>
                <asp:ObjectDataSource ID="odsSort" runat="server" DeleteMethod="Delete" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetData" TypeName="DataAccess.SortForMaintDataSetTableAdapters.GetALLSortForMaintTableAdapter"
                    UpdateMethod="Update">
                    <DeleteParameters>
                        <asp:Parameter Name="plant_code" Type="String" />
                        <asp:Parameter Name="section_code" Type="String" />
                        <asp:Parameter Name="sort_code" Type="String" />
                    </DeleteParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="plant_code" Type="String" />
                        <asp:Parameter Name="section_code" Type="String" />
                        <asp:Parameter Name="sort_code" Type="String" />
                        <asp:Parameter Name="sort_desc" Type="String" />
                        <asp:Parameter Name="sequence" Type="Int32" />
                        <asp:Parameter Name="required_flag" Type="String" />
                    </UpdateParameters>
                    <SelectParameters>
                        <asp:ControlParameter ControlID="fltPlant" Name="plant_code" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:ControlParameter ControlID="fltSection" Name="section_code" PropertyName="SelectedValue"
                            Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
        </tr>
        <tr>
            <td>
                <p>
                    <asp:Label ID="lbMessage" runat="server" CssClass="Msg"></asp:Label>
                </p>
            </td>
        </tr>
        <tr>
            <td style="height: 70px">
                <table style="padding: 2px; background-color: #669999" width="500">
                    <tr>
                        <td style="width: 100; height: 21px;">
                            Plant</td>
                        <td style="width: 400; height: 21px;">
                            <asp:DropDownList ID="fltPlant" runat="server" Width="350px" DataSourceID="odsPlant"
                                DataTextField="plant_name" DataValueField="plant_code" AutoPostBack="True" CausesValidation="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px; height: 21px">
                            Section</td>
                        <td style="width: 400px; height: 21px">
                            <asp:DropDownList ID="fltSection" runat="server" Width="350px" DataSourceID="odsSection"
                                DataTextField="section_desc" DataValueField="section_code" AutoPostBack="True"
                                CausesValidation="True">
                            </asp:DropDownList></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
                <asp:GridView ID="gvSort" runat="server" AutoGenerateColumns="False" DataSourceID="odsSort"
                    AllowPaging="True" PageSize="50" ForeColor="Black">
                    <Columns>
                        <asp:TemplateField HeaderText="Sort Code">
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                                <asp:TextBox ID="txtSortCode" runat="server" Text='<%# Eval("sort_code") %>'
                                    Width="200px" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Sort Desc">
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                                <asp:TextBox ID="txtSortDesc" runat="server" Text='<%# Eval("sort_desc") %>'
                                    Width="200px" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Sequence">
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                                <asp:TextBox ID="txtSequence" runat="server" Text='<%# Eval("sequence") %>' Width="50px" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Required">
                            <ItemStyle CssClass="dataGridContent" HorizontalAlign="Center" BorderStyle="None" />
                            <ItemTemplate>
                                <asp:CheckBox ID="chkRequired" runat="server" Checked='<%# Eval("required_flag") %>'
                                    Enabled="true"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemStyle BorderStyle="None" />
                            <ItemTemplate>
                                <asp:Button ID="btnEdit" runat="server" Text='<%# IIF(EVAL("sort_code") = "","Save","Update") %>'
                                    CommandName="EditSort" CommandArgument='<%#  CType(Container,GridViewRow).RowIndex  %>' />&nbsp;&nbsp;
                                <asp:Button ID="btnDelete" runat="server" Text="Delete" Visible='<%# IIF(EVAL("sort_code") = "","False","True") %>'
                                    CommandName="DeleteSort" CommandArgument='<%#  CType(Container,GridViewRow).RowIndex  %>'
                                    OnClientClick="return confirm('Are you sure you want to delete?');" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle BackColor="#669999" Font-Bold="True" ForeColor="Black" />
                    <AlternatingRowStyle BackColor="#E0E0E0" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnCancel" runat="server" Text="Back"></asp:Button>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr style="height: 100%">
            <td>
            </td>
        </tr>
    </table>
</asp:Content>
