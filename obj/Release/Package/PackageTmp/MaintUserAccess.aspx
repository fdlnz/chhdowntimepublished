<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/TimberPanels.Master" Codebehind="MaintUserAccess.aspx.vb"
    Inherits="Downtime.MaintUserAccess" Title="Downtime: Maintain User Access" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
    Maintain User Access
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <table style="width: 100%;" class="content">
        <tr>
            <td>
                <asp:ObjectDataSource ID="odsPlant" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetData" TypeName="DataAccess.GetALLPlantForUserAccessTableAdapters.GetALLPlantForUserAccessTableAdapter">
                    <SelectParameters>
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
        </tr>
        <tr>
            <td>
                <asp:ObjectDataSource ID="odsSection" runat="server" SelectMethod="GetData" TypeName="DataAccess.SectionListDataSetTableAdapters.GetSectionByPlantTableAdapter"
                    OldValuesParameterFormatString="original_{0}">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="fltPlant" Name="plant_code" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:Parameter DefaultValue="Y" Name="AddBlankEntry" Type="String" />
                        <asp:Parameter DefaultValue="" Name="BlankEntryName" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
        </tr>
        <tr>
            <td>
                <asp:ObjectDataSource ID="odsPlantAdministrator" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetData" TypeName="DataAccess.PlantAdministratorForMaintDataSetTableAdapters.GetALLPlantAdministratorsForMaintTableAdapter">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="fltPlant" Name="plant_code" PropertyName="SelectedValue"
                            Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
        </tr>
        <tr>
            <td>
                <asp:ObjectDataSource ID="odsUserAccess" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetData" TypeName="DataAccess.UserAccessForMaintDataSetTableAdapters.GetALLUserAccessForMaintTableAdapter">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="fltPlant" Name="plant_code" PropertyName="SelectedValue"
                            Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
        </tr>
        <tr>
            <td>
                <p>
                    <asp:Label ID="lbMessage" runat="server" CssClass="Msg"></asp:Label>
                </p>
            </td>
        </tr>
        <tr>
            <td>
                <table style="padding: 2px; background-color: #669999" width="500">
                    <tr>
                        <td style="width: 100;">
                            Plant</td>
                        <td style="width: 400;">
                            <asp:DropDownList ID="fltPlant" runat="server" Width="350px" DataSourceID="odsPlant"
                                DataTextField="plant_name" DataValueField="plant_code" AutoPostBack="True" CausesValidation="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
                <asp:GridView ID="gvPlantAdministrator" runat="server" AutoGenerateColumns="False" DataSourceID="odsPlantAdministrator"
                    AllowPaging="True" PageSize="50" ForeColor="Black">
                    <Columns>
                        <asp:TemplateField HeaderText="Hidden" InsertVisible="False" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbOldUserNamePA" runat="server" Text='<%# Bind("user_name") %>' Visible="False"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Plant Administrator">
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                                <asp:TextBox ID="txtNewUserNamePA" runat="server" Text='<%# Eval("user_name") %>' Width="200px" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Plant" InsertVisible="False">
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                                <asp:TextBox ID="txtPlantCodePA" runat="server" Text='<%# Bind("plant_code") %>' Width="50px"
                                    ReadOnly="true"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemStyle BorderStyle="None" />
                            <ItemTemplate>
                                <asp:Button ID="btnEditPA" runat="server" Text='<%# IIF(EVAL("user_name") = "","Save(Plant Admin)","Update(Plant Admin)") %>'
                                    CommandName="EditPlantAdministrator" CommandArgument='<%#  CType(Container,GridViewRow).RowIndex  %>' />&nbsp;&nbsp;
                                <asp:Button ID="btnDeletePA" runat="server" Text="Delete(Plant Admin)" Visible='<%# IIF(EVAL("user_name") = "","False","True") %>'
                                    CommandName="DeletePlantAdministrator" CommandArgument='<%#  CType(Container,GridViewRow).RowIndex  %>'
                                    OnClientClick="return confirm('Are you sure you want to delete?');" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle BackColor="#669999" Font-Bold="True" ForeColor="Black" />
                    <AlternatingRowStyle BackColor="#E0E0E0" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
                <asp:GridView ID="gvUserAccess" runat="server" AutoGenerateColumns="False" DataSourceID="odsUserAccess"
                    AllowPaging="True" PageSize="50" ForeColor="Black">
                    <Columns>
                        <asp:TemplateField HeaderText="Hidden" InsertVisible="False" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbOldUserName" runat="server" Text='<%# Bind("user_name") %>' Visible="False"></asp:Label>
                                <asp:Label ID="lbOldSectionCode" runat="server" Text='<%# Bind("section_code") %>'
                                    Visible="False"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="User">
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                                <asp:TextBox ID="txtNewUserName" runat="server" Text='<%# Eval("user_name") %>' Width="200px" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Plant" InsertVisible="False">
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                                <asp:TextBox ID="txtPlantCode" runat="server" Text='<%# Bind("plant_code") %>' Width="50px"
                                    ReadOnly="true"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Section">
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                                <asp:DropDownList ID="dplNewSection" runat="server" Width="250px" DataSourceID="odsSection"
                                    DataTextField="section_desc" DataValueField="section_code" SelectedValue='<%# Eval("section_code") %>'
                                    OnSelectedIndexChanged="dplNewSection_SelectedIndexChanged"  AutoPostBack="true">
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Default">
                            <ItemStyle CssClass="dataGridContent" HorizontalAlign="Center" BorderStyle="None" />
                            <ItemTemplate>
                                <asp:CheckBox ID="chkDefault" runat="server" Checked='<%# Eval("default_flag") %>' Enabled='<%# IIF(EVAL("section_code") = "All","False","True") %>'></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemStyle BorderStyle="None" />
                            <ItemTemplate>
                                <asp:Button ID="btnEdit" runat="server" Text='<%# IIF(EVAL("user_name") = "","Save","Update") %>'
                                    CommandName="EditUserAccess" CommandArgument='<%#  CType(Container,GridViewRow).RowIndex  %>' />&nbsp;&nbsp;
                                <asp:Button ID="btnDelete" runat="server" Text="Delete" Visible='<%# IIF(EVAL("user_name") = "","False","True") %>'
                                    CommandName="DeleteUserAccess" CommandArgument='<%#  CType(Container,GridViewRow).RowIndex  %>'
                                    OnClientClick="return confirm('Are you sure you want to delete?');" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle BackColor="#669999" Font-Bold="True" ForeColor="Black" />
                    <AlternatingRowStyle BackColor="#E0E0E0" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnCancel" runat="server" Text="Back"></asp:Button>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr style="height: 100%">
            <td>
            </td>
        </tr>
    </table>
</asp:Content>
