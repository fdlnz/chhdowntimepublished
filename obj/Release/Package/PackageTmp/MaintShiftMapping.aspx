<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/TimberPanels.Master" Codebehind="MaintShiftMapping.aspx.vb"
    Inherits="Downtime.MaintShiftMapping" Title="Downtime: Maintain Shift Mapping" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
    Maintain Shift Mapping
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <table style="width: 100%;" class="content">
        <tr>
            <td>
                <asp:ObjectDataSource ID="odsPlant" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetData" TypeName="DataAccess.PlantListDataSetTableAdapters.GetALLPlantTableAdapter">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="N" Name="AddBlankEntry" Type="String" />
                        <asp:Parameter DefaultValue="" Name="BlankEntryName" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="odsSection" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetData" TypeName="DataAccess.SectionListDataSetTableAdapters.GetSectionByPlantTableAdapter">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="fltPlant" Name="plant_code" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:Parameter DefaultValue="N" Name="AddBlankEntry" Type="String" />
                        <asp:Parameter DefaultValue="" Name="BlankEntryName" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="odsDayOfWeek" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetData" TypeName="DataAccess.DayOfWeekDataSetTableAdapters.GetALLDayOfWeekTableAdapter">
                </asp:ObjectDataSource>
            </td>
        </tr>
        <tr>
            <td>
                <asp:ObjectDataSource ID="odsShiftMapping" runat="server" SelectMethod="GetData"
                    TypeName="DataAccess.ShiftMappingForMaintDataSetTableAdapters.GetALLShiftMappingForMaintTableAdapter"
                    OldValuesParameterFormatString="original_{0}" DeleteMethod="Delete" UpdateMethod="Update">
                    <DeleteParameters>
                        <asp:Parameter Name="plant_code" Type="String" />
                        <asp:Parameter Name="Shift" Type="String" />
                        <asp:Parameter Name="section_code" Type="String" />
                        <asp:Parameter Name="day_of_week" Type="Int32" />
                        <asp:Parameter Name="machine_code" Type="String" />
                        <asp:Parameter Direction="InputOutput" Name="Action" Type="String" />
                    </DeleteParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="plant_code" Type="String" />
                        <asp:Parameter Name="shift" Type="String" />
                        <asp:Parameter Name="section_code" Type="String" />
                        <asp:Parameter Name="day_of_week" Type="Int32" />
                        <asp:Parameter Name="start_time" Type="DateTime" />
                        <asp:Parameter Name="end_time" Type="DateTime" />						
                        <asp:Parameter Name="machine_code" Type="String" />
                        <asp:Parameter Name="meal_break" Type="Int32" />
						<asp:Parameter Name="sun" Type="Boolean" />
						<asp:Parameter Name="mon" Type="Boolean" />
						<asp:Parameter Name="tue" Type="Boolean" />
						<asp:Parameter Name="wed" Type="Boolean" />
						<asp:Parameter Name="thu" Type="Boolean" />
						<asp:Parameter Name="fri" Type="Boolean" />
						<asp:Parameter Name="sat" Type="Boolean" />
                        <asp:Parameter Direction="InputOutput" Name="action" Type="String" />
                    </UpdateParameters>
                    <SelectParameters>
                        <asp:ControlParameter ControlID="fltPlant" Name="plant_code" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:ControlParameter ControlID="fltSection" Name="section_code" PropertyName="SelectedValue"
                            Type="string" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
        </tr>
        <tr>
            <td>
                <p>
                    <asp:Label ID="lbMessage" runat="server" CssClass="Msg"></asp:Label>
                </p>
            </td>
        </tr>
        <tr>
            <td>
                <table style="padding: 2px; background-color: #669999" width="500">
                    <tr>
                        <td style="width: 100;">
                            Plant</td>
                        <td style="width: 400;">
                            <asp:DropDownList ID="fltPlant" runat="server" Width="350px" DataSourceID="odsPlant"
                                DataTextField="plant_name" DataValueField="plant_code" AutoPostBack="True" CausesValidation="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100;">
                            Section</td>
                        <td style="width: 400;">
                            <asp:DropDownList ID="fltSection" runat="server" Width="350px" DataSourceID="odsSection"
                                DataTextField="section_desc" DataValueField="section_code" AutoPostBack="True"
                                CausesValidation="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
                <asp:GridView ID="gvShift" runat="server" AutoGenerateColumns="False" DataSourceID="odsShiftMapping"
                    AllowPaging="True" PageSize="50" ForeColor="Black">
                    <Columns>
                        <asp:TemplateField HeaderText="Hidden" InsertVisible="False" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbShift" runat="server" Text='<%# Bind("Shift") %>' Visible="False"></asp:Label>
                                <asp:Label ID="lbSectionCode" runat="server" Text='<%# Bind("section_code") %>' Visible="False"></asp:Label>
                                <asp:Label ID="lbPlant" runat="server" Text='<%# Bind("plant_code") %>' Visible="False"></asp:Label>
                                <asp:Label ID="lbMachineCode" runat="server" Text='<%# Bind("machine_code") %>' Visible="False"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Plant">
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                                <asp:Label ID="lbPlantCode" runat="server" Text='<%# Bind("plant_code") %>' Width="50px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Section">
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                                <asp:DropDownList ID="dplSection" runat="server" DataSourceID="odsSection" DataTextField="section_desc"
                                    DataValueField="section_code" Width="200px">
                                </asp:DropDownList>
                                <asp:ObjectDataSource ID="odsSection" runat="server" OldValuesParameterFormatString="original_{0}"
                                    SelectMethod="GetData" TypeName="DataAccess.SectionListDataSetTableAdapters.GetSectionByPlantTableAdapter">
                                    <SelectParameters>
                                        <asp:ControlParameter ControlID="fltPlant" Name="plant_code" PropertyName="SelectedValue"
                                            Type="String" />
                                        <asp:Parameter Name="AddBlankEntry" Type="String" />
                                        <asp:Parameter Name="BlankEntryName" Type="String" />
                                    </SelectParameters>
                                </asp:ObjectDataSource>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Machine">
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                                <asp:DropDownList ID="dplMachine" runat="server" DataSourceID="odsMachine" DataTextField="machine_desc"
                                    DataValueField="machine_code" Width="200px">
                                </asp:DropDownList>
                                <asp:ObjectDataSource ID="odsMachine" runat="server" OldValuesParameterFormatString="original_{0}"
                                    SelectMethod="GetData" TypeName="DataAccess.MachineListDataSetTableAdapters.GetMachineBySectionTableAdapter">
                                    <SelectParameters>
                                        <asp:ControlParameter ControlID="fltPlant" Name="plant_code" PropertyName="SelectedValue"
                                            Type="String" />
                                        <asp:ControlParameter ControlID="fltSection" Name="section_code" PropertyName="SelectedValue"
                                            Type="String" />
                                        <asp:Parameter Name="AddBlankEntry" Type="String" />
                                        <asp:Parameter Name="BlankEntryName" Type="String" />
                                    </SelectParameters>
                                </asp:ObjectDataSource>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Shift">
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                                <asp:DropDownList ID="dplShift" runat="server" DataSourceID="odsShift" DataValueField="shift"
                                    Width="100px">
                                </asp:DropDownList>
                                <asp:ObjectDataSource ID="odsShift" runat="server" OldValuesParameterFormatString="original_{0}"
                                    SelectMethod="GetData" TypeName="DataAccess.ShiftListDataSetTableAdapters.GetShiftByPlantTableAdapter">
                                    <SelectParameters>
                                        <asp:ControlParameter ControlID="fltPlant" Name="plant_code" PropertyName="SelectedValue"
                                            Type="String" />
                                        <asp:Parameter Name="AddBlankEntry" Type="String" />
                                        <asp:Parameter Name="BlankEntryName" Type="String" />
                                    </SelectParameters>
                                </asp:ObjectDataSource>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Day of Week" Visible="False">
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                                <asp:DropDownList ID="dplDayOfWeek" runat="server" DataSourceID="odsDayOfWeek" DataTextField="day_of_week_desc"
                                    DataValueField="day_of_week" Enabled='<%# IIF(EVAL("section_code") = "","True","False") %>'>
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>						
                        <asp:TemplateField HeaderText="Start Time">
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:TextBox ID="txtStartHour" runat="server" Width="15px" Text='<%# Eval("start_time", "{0:HH}") %>' /><asp:RangeValidator
                                    ID="rvStartHour" runat="server" ErrorMessage="*" MaximumValue="23" MinimumValue="0"
                                    ControlToValidate="txtStartHour" Display="Dynamic" Type="Integer"></asp:RangeValidator>:<asp:TextBox
                                        ID="txtStartMinute" runat="server" Width="15px" Text='<%# Eval("start_time", "{0:mm}") %>' /><asp:RangeValidator
                                            ID="rvStartMinute" runat="server" ErrorMessage="*" MaximumValue="59" MinimumValue="0"
                                            ControlToValidate="txtStartMinute" Display="Dynamic" Type="Integer"></asp:RangeValidator>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="End Time">
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:TextBox ID="txtEndHour" runat="server" Width="15px" Text='<%# Eval("end_time", "{0:HH}") %>' /><asp:RangeValidator
                                    ID="rvEndHour" runat="server" ErrorMessage="*" MaximumValue="23" MinimumValue="0"
                                    ControlToValidate="txtEndHour" Display="Dynamic" Type="Integer"></asp:RangeValidator>:<asp:TextBox
                                        ID="txtEndMinute" runat="server" Width="15px" Text='<%# Eval("end_time", "{0:mm}") %>' /><asp:RangeValidator
                                            ID="rvEndMinute" runat="server" ErrorMessage="*" MaximumValue="59" MinimumValue="0"
                                            ControlToValidate="txtEndMinute" Display="Dynamic" Type="Integer"></asp:RangeValidator>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Meal Break">
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:TextBox ID="txtMealBreak" runat="server" Width="30px" Text='<%# Bind("meal_break") %>'></asp:TextBox>
                                <asp:CompareValidator ID="cvTxtMealBreak" runat="server" ErrorMessage="Meal break cannot be less than 0" ControlToValidate="txtMealBreak"
                                Type="Integer" Operator="GreaterThan" ValueToCompare="-1" Display="Dynamic">*</asp:CompareValidator>
                            </ItemTemplate>
                        </asp:TemplateField>
						<asp:TemplateField HeaderText="Sun">
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSun" runat="server" Checked='<%# IIf(Eval("sun"), "True", "False")%>'>
                                </asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateField>
						<asp:TemplateField HeaderText="Mon">
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                                <asp:CheckBox ID="chkMon" runat="server" Checked='<%# IIf(Eval("mon"), "True", "False")%>'>
                                </asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateField>
						<asp:TemplateField HeaderText="Tue">
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                                <asp:CheckBox ID="chkTue" runat="server" Checked='<%# IIf(Eval("tue"), "True", "False")%>'>
                                </asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateField>
						<asp:TemplateField HeaderText="Wed">
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                                <asp:CheckBox ID="chkWed" runat="server" Checked='<%# IIf(Eval("wed"), "True", "False")%>'>
                                </asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateField>
						<asp:TemplateField HeaderText="Thu">
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                                <asp:CheckBox ID="chkThu" runat="server" Checked='<%# IIf(Eval("thu"), "True", "False")%>'>
                                </asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateField>
						<asp:TemplateField HeaderText="Fri">
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                                <asp:CheckBox ID="chkFri" runat="server" Checked='<%# IIf(Eval("fri"), "True", "False")%>'>
                                </asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateField>
						<asp:TemplateField HeaderText="Sat">
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSat" runat="server" Checked='<%# IIf(Eval("sat"), "True", "False")%>'>
                                </asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button ID="btnEdit" runat="server" Text='<%# IIF(EVAL("Shift") = "","Save","Update") %>'
                                    CommandName="EditShift" CommandArgument='<%#  CType(Container,GridViewRow).RowIndex  %>' />&nbsp;&nbsp;
                                <asp:Button ID="btnDelete" runat="server" Text="Delete" Visible='<%# IIF(EVAL("Shift") = "","False","True") %>'
                                    CommandName="DeleteShift" CommandArgument='<%#  CType(Container,GridViewRow).RowIndex  %>'
                                    OnClientClick="return confirm('Are you sure you want to delete?');" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle BackColor="#669999" Font-Bold="True" ForeColor="Black" />
                    <AlternatingRowStyle BackColor="#E0E0E0" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnCancel" runat="server" Text="Back"></asp:Button>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr style="height: 100%">
            <td>
            </td>
        </tr>
    </table>
</asp:Content>
