<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/TimberPanels.Master" CodeBehind="MaintSection.aspx.vb" Inherits="Downtime.MaintSection" 
    title="Downtime: Maintain Section" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
    Maintain Section
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <table style="width: 100%;" class="content">
	     <tr>
			<td>
			<asp:ObjectDataSource ID="odsPlant" runat="server" OldValuesParameterFormatString="original_{0}"
             SelectMethod="GetData" TypeName="DataAccess.PlantListDataSetTableAdapters.GetALLPlantTableAdapter">
             <SelectParameters>
                 <asp:Parameter DefaultValue="N" Name="AddBlankEntry" Type="String" />
                 <asp:Parameter DefaultValue="" Name="BlankEntryName" Type="String" />
               </SelectParameters>
               </asp:ObjectDataSource>
  
             </td>
		 </tr>
	     <tr>
			<td >
			<asp:ObjectDataSource ID="odsSection" runat="server" OldValuesParameterFormatString="original_{0}"
             SelectMethod="GetData" TypeName="DataAccess.SectionForMaintDataSetTableAdapters.GetALLSectionForMaintTableAdapter">
                <SelectParameters>
                    <asp:ControlParameter ControlID="fltPlant" Name="plant_code" PropertyName="SelectedValue"
                        Type="String" />
                </SelectParameters>
               </asp:ObjectDataSource>
             </td>
		  </tr>
		  <tr>
		    <td >
                <p><asp:Label ID="lbMessage" runat="server" CssClass="Msg"></asp:Label>
                </p>
            </td>
          </tr>
          <tr>
            <td>
                <table style="padding: 2px; background-color: #669999" width="500">
		            <tr> 
		            <td style="width: 100;">Plant</td>
		            <td style="width: 400;"><asp:dropdownlist id="fltPlant" runat="server" Width="350px"  
                    DataSourceID="odsPlant" DataTextField="plant_name" 
                    DataValueField="plant_code" AutoPostBack="True" CausesValidation="True" data></asp:dropdownlist>
                    </td>
                    </tr>		
                </table>
            </td>
          </tr> 
	      <tr>
		    <td>&nbsp;
		      <asp:GridView ID="gvSection" runat="server" AutoGenerateColumns="False" 
                    DataSourceID="odsSection" AllowPaging="True" PageSize="50" ForeColor="Black">
                    <Columns>
                        <asp:TemplateField HeaderText="Plant Code" InsertVisible="False"  >
                             <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                                <asp:Label ID="lbPlantCode" runat="server" Text='<%# Bind("plant_code") %>' Width="50px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Section Code" InsertVisible="False"  >
                             <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                                <asp:Label ID="lbSectionCode" runat="server" Text='<%# Bind("section_code") %>' Width="50px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Section Desc" >
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                            <asp:textBox ID="txtSectionDesc" runat="server" Text='<%# Eval("section_desc") %>' Width="300px"/>                            
                            </ItemTemplate>
                        </asp:TemplateField>  
                        <asp:TemplateField HeaderText="Sequence"  >
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None"  />
                            <ItemTemplate>
                            <asp:textBox ID="txtSequence" runat="server" Text='<%# Eval("sequence") %>' Width="50px"/>                            
                            </ItemTemplate>
                        </asp:TemplateField>  
                        <asp:TemplateField HeaderText="Required" >
                            <ItemStyle CssClass="dataGridContent" HorizontalAlign="Center" BorderStyle="None"  />
                            <ItemTemplate>
                             <asp:CheckBox ID="chkRequired" runat="server" Checked='<%# Eval("required_flag") %>' ></asp:CheckBox>                            
                            </ItemTemplate>
                        </asp:TemplateField>  
                        <asp:TemplateField HeaderText="PLC" >
                            <ItemStyle CssClass="dataGridContent" HorizontalAlign="Center" BorderStyle="None"  />
                            <ItemTemplate>
                             <asp:CheckBox ID="chkPlcFlag" runat="server" Checked='<%# Eval("plc_flag") %>'  ></asp:CheckBox>                            
                            </ItemTemplate>
                        </asp:TemplateField>  
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button ID="btnEdit" runat="server" Text='Update' 
                                CommandName="EditSection" CommandArgument='<%#  CType(Container,GridViewRow).RowIndex %>' />
                            </ItemTemplate>
                             <ItemStyle BorderStyle="None" />
                        </asp:TemplateField>    
                    </Columns>                   
                    <HeaderStyle BackColor="#669999" Font-Bold="True" ForeColor="Black" />
                    <AlternatingRowStyle BackColor="#E0E0E0" />
                </asp:GridView>
			    
		    </td>
		</tr>
		<tr >
			<td>
			<asp:button id="btnCancel" runat="server" Text="Back"></asp:button>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr style="height: 100%">
			<td ></td>
		</tr>
	</table>
</asp:Content>