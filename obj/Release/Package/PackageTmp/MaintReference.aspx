<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/TimberPanels.Master" Codebehind="MaintReference.aspx.vb"
    Inherits="Downtime.MaintReference" Title="Downtime: Maintenance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
    Maintenance
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <table style="width: 100%;" class="content">
        <tr>
            <td>
                <p>
                    <asp:Label ID="lbMessage" runat="server" CssClass="Msg"></asp:Label></p>
                <br />
            </td>
        </tr>
        <tr>
            <td>
                <asp:LinkButton ID="btnRefreshMasterData" runat="server" Text='Refresh SAP FL Master Data &raquo;'
                    CssClass="linkButton"></asp:LinkButton></td>
        </tr>
        <tr>
            <td>
                <asp:LinkButton ID="btnCrew" runat="server" Text='Maintain Crew &raquo;' CssClass="linkButton"></asp:LinkButton></td>
        </tr>
        <tr>
            <td>
                <asp:LinkButton ID="btnShift" runat="server" Text='Maintain Shift &raquo;' CssClass="linkButton"></asp:LinkButton></td>
        </tr>
          <tr>
            <td>
                <asp:LinkButton ID="btnShiftMapping" runat="server" Text='Maintain Shift Mapping &raquo;' CssClass="linkButton"></asp:LinkButton></td>
        </tr>
        <tr>
            <td>
                <asp:LinkButton ID="btnPlant" runat="server" Text='Maintain Plant &raquo;' CssClass="linkButton"></asp:LinkButton></td>
        </tr>
        <tr>
            <td>
                <asp:LinkButton ID="btnSection" runat="server" Text='Maintain Section &raquo;' CssClass="linkButton"></asp:LinkButton></td>
        </tr>
        <tr>
            <td>
                <asp:LinkButton ID="btnMachine" runat="server" Text='Maintain Machine &raquo;' CssClass="linkButton"></asp:LinkButton></td>
        </tr>
        <tr>
            <td>
                <asp:LinkButton ID="btnSubArea" runat="server" Text='Maintain Sub Area &raquo;' CssClass="linkButton"></asp:LinkButton></td>
        </tr>
         <tr>
            <td>
                <asp:LinkButton ID="btnSubAreaItem" runat="server" Text='Maintain Sub Area Item &raquo;' CssClass="linkButton"></asp:LinkButton></td>
        </tr>
        <tr>
            <td>
                <asp:LinkButton ID="btnFault" runat="server" Text='Maintain Fault &raquo;' CssClass="linkButton"></asp:LinkButton></td>
        </tr>
        <tr>
            <td>
                <asp:LinkButton ID="btnSort" runat="server" Text='Maintain Sort &raquo;' CssClass="linkButton"></asp:LinkButton></td>
        </tr>
        <tr>
            <td>
                <asp:LinkButton ID="btnLength" runat="server" Text='Maintain Length &raquo;' CssClass="linkButton"></asp:LinkButton></td>
        </tr>
             <tr>
            <td>
                <asp:LinkButton ID="btnProduct" runat="server" Text='Maintain Product &raquo;' CssClass="linkButton"></asp:LinkButton></td>
        </tr>
        <tr>
            <td>
                <asp:LinkButton ID="btnLostTime" runat="server" CssClass="linkButton">Maintain Lost Time Groups &raquo;</asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td>
                <asp:LinkButton ID="btnUserAccess" runat="server" Text='Maintain User Access &raquo;'
                    CssClass="linkButton"></asp:LinkButton></td>
        </tr>
        <tr>
            <td>
                <asp:LinkButton ID="btnAdministrator" runat="server" Text='Maintain Administrator &raquo;'
                    CssClass="linkButton"></asp:LinkButton></td>
        </tr>

        <tr>
            <td>
                <asp:LinkButton ID="btnUploadShiftDuration" runat="server" Text='Upload Shift Duration &raquo;'
                    CssClass="linkButton"></asp:LinkButton>

            </td>
        </tr>
    </table>
</asp:Content>
