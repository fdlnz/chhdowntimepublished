﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/TimberPanels.Master" CodeBehind="MaintDowntimeTimber.aspx.vb" Inherits="Downtime.MaintDowntimeTimber" Title="Timber Downtime: Date Entry/Edit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script language="javascript" type="text/javascript">
        function TABLE1_onclick() {

        }
        function ConfirmMessagePopUp() {

            var conf = confirm("Are you sure you want to update the Crew for all existing records of this shift?");
            if (conf == true) {
                return true;
            } else {
                return false;
            }
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
    Timber Downtime Data Entry / Edit &nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Button ID="btnRefresh" runat="server" Text="Refresh" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <table style="width: 100%;" class="content">
        <tr>
            <td>
                <asp:Label ID="lbMessage" runat="server"></asp:Label>
                <asp:ObjectDataSource ID="odsPlant" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetData" TypeName="DataAccess.ValidPlantListDataSetTableAdapters.GetALLValidPlantTableAdapter">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="Y" Name="AddBlankEntry" Type="String" />
                        <asp:Parameter DefaultValue="" Name="BlankEntryName" Type="String" />
                        <asp:SessionParameter Name="CurrentUser" SessionField="current_user" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="odsSection" runat="server" SelectMethod="GetData" TypeName="DataAccess.ValidSectionListDataSetTableAdapters.GetValidSectionByPlantTableAdapter"
                    OldValuesParameterFormatString="original_{0}">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="fltPlant" DefaultValue="Session(&quot;current_user_default_plant&quot;)"
                            Name="plant_code" PropertyName="SelectedValue" Type="String" />
                        <asp:Parameter DefaultValue="N" Name="AddBlankEntry" Type="String" />
                        <asp:Parameter DefaultValue="" Name="BlankEntryName" Type="String" />
                        <asp:SessionParameter Name="CurrentUser" SessionField="current_user" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="odsMachine" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetData" TypeName="DataAccess.ValidMachineListDataSetTableAdapters.GetValidMachineBySectionTableAdapter">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="fltPlant" Name="plant_code" PropertyName="SelectedValue"
                            Type="String" DefaultValue="Session(&quot;current_user_default_plant&quot;)" />
                        <asp:ControlParameter ControlID="fltSection" Name="section_code" PropertyName="SelectedValue"
                            Type="String" DefaultValue="Session(&quot;current_user_default_section&quot;)" />
                        <asp:Parameter DefaultValue="N" Name="AddBlankEntry" Type="String" />
                        <asp:Parameter Name="BlankEntryName" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="odsSubArea" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetData" TypeName="DataAccess.ValidSubAreaDataSetTableAdapters.GetValidSubAreaByMachineTableAdapter">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="fltPlant" Name="plant_code" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:ControlParameter ControlID="fltSection" Name="section_code" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:ControlParameter ControlID="fltMachine" Name="machine_code" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:Parameter DefaultValue="Y" Name="AddBlankEntry" Type="String" />
                        <asp:Parameter Name="BlankEntryName" Type="String" DefaultValue="" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="odsFullSubArea" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetData" TypeName="DataAccess.SubAreaListDataSetTableAdapters.GetSubAreaByMachineTableAdapter">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="fltPlant" Name="plant_code" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:ControlParameter ControlID="fltSection" Name="section_code" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:ControlParameter ControlID="fltMachine" Name="machine_code" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:Parameter DefaultValue="Y" Name="AddBlankEntry" Type="String" />
                        <asp:Parameter Name="BlankEntryName" Type="String" DefaultValue="" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="odsCrew" runat="server" SelectMethod="GetData" TypeName="DataAccess.CrewListDataSetTableAdapters.GetCrewByPlantTableAdapter"
                    OldValuesParameterFormatString="original_{0}">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="fltPlant" Name="plant_code" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:Parameter DefaultValue="N" Name="AddBlankEntry" Type="String" />
                        <asp:Parameter DefaultValue=" " Name="BlankEntryName" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="odsShift" runat="server" SelectMethod="GetData" TypeName="DataAccess.ShiftListDataSetTableAdapters.GetShiftByPlantTableAdapter"
                    OldValuesParameterFormatString="original_{0}">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="fltPlant" Name="plant_code" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:Parameter DefaultValue="N" Name="AddBlankEntry" Type="String" />
                        <asp:Parameter DefaultValue=" " Name="BlankEntryName" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="odsHour" runat="server" SelectMethod="GetData" TypeName="DataAccess.HourListDataSetTableAdapters.GetALLHourTableAdapter"
                    OldValuesParameterFormatString="original_{0}">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="Y" Name="AddBlankEntry" Type="String" />
                        <asp:Parameter DefaultValue="" Name="BlankEntryName" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="odsLostTime" runat="server" SelectMethod="GetData" TypeName="DataAccess.LostTimeTableAdapters.GetLostTimeTableAdapter"
                    OldValuesParameterFormatString="original_{0}">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="Y" Name="AddBlankEntry" Type="String" />
                        <asp:Parameter DefaultValue="" Name="BlankEntryName" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="odsShiftDuration" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetData" TypeName="DataAccess.MachineShiftDurationTableAdapters.FindMachineShiftDurationTableAdapter" UpdateMethod="Update">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="fltPlant" Name="plant_code" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:ControlParameter ControlID="fltSection" Name="section_code" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:ControlParameter ControlID="fltMachine" Name="machine_code" PropertyName="SelectedValue" Type="String" />
                        <asp:ControlParameter ControlID="txtDate" Name="downtime_date" PropertyName="Text"
                            Type="DateTime" />
                        <asp:ControlParameter ControlID="fltShift" Name="shift" PropertyName="SelectedValue"
                            Type="String" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="machine_shift_duration_id" Type="Int32" />
                        <asp:Parameter Name="plant_code" Type="String" />
                        <asp:Parameter Name="section_code" Type="String" />
                        <asp:Parameter Name="machine_code" Type="String" />
                        <asp:Parameter Name="downtime_date" Type="DateTime" />
                        <asp:Parameter Name="shift" Type="String" />
                        <asp:Parameter Name="machine_shift_duration" Type="Int32" />
                    </UpdateParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="odsDowntime" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetData" TypeName="DataAccess.DowntimeListDataSetTableAdapters.FindDowntimeTableAdapter"
                    DeleteMethod="Delete" UpdateMethod="Update">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="fltPlant" Name="plant_code" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:ControlParameter ControlID="fltSection" Name="section_code" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:ControlParameter ControlID="fltMachine" Name="machine_code" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:ControlParameter ControlID="txtDate" Name="downtime_date" PropertyName="Text"
                            Type="DateTime" />
                        <asp:ControlParameter ControlID="fltShift" Name="shift" PropertyName="SelectedValue"
                            Type="String" />
                    </SelectParameters>
                    <DeleteParameters>
                        <asp:Parameter Name="downtime_id" Type="Int32" />
                    </DeleteParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="downtime_id" Type="Int32" />
                        <asp:Parameter Name="plant_code" Type="String" />
                        <asp:Parameter Name="section_code" Type="String" />
                        <asp:Parameter Name="machine_code" Type="String" />
                        <asp:Parameter Name="sub_area_code" Type="String" />
                        <asp:Parameter Name="downtime_date" Type="DateTime" />
                        <asp:Parameter Name="hour" Type="Int32" />
                        <asp:Parameter Name="crew" Type="String" />
                        <asp:Parameter Name="shift" Type="String" />
                        <asp:Parameter Name="fault_key" Type="String" />
                        <asp:Parameter Name="time_Lost" Type="Decimal" />
                        <asp:Parameter Name="comment" Type="String" />
                        <asp:Parameter Name="UserName" Type="String" />
                        <asp:Parameter Name="occurrence" Type="String" />
                        <asp:Parameter Name="sort_code" Type="String" />
                        <asp:Parameter Name="length_code" Type="String" />
                        <asp:Parameter Name="sub_area_item_code" Type="String" />
                        <asp:Parameter Name="start_time" Type="DateTime" />
                        <asp:Parameter Name="product_key" Type="Int32" />
                        <asp:Parameter Name="lost_time_group" Type="String" />
                    </UpdateParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="odsDowntimeForShift" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetData" TypeName="DataAccess.DowntimeListForShiftDatasetTableAdapters.FindDowntimeForShiftTableAdapter">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="fltPlant" Name="plant_code" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:ControlParameter ControlID="fltSection" Name="section_code" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:ControlParameter ControlID="txtDate" Name="downtime_date" PropertyName="Text"
                            Type="DateTime" />
                        <asp:ControlParameter ControlID="fltShift" Name="shift" PropertyName="SelectedValue"
                            Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="odsSort" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetData" TypeName="DataAccess.SortListDataSetTableAdapters.GetSortBySectionTableAdapter">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="fltPlant" Name="plant_code" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:ControlParameter ControlID="fltSection" Name="section_code" PropertyName="SelectedValue"
                            Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="odsLength" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetData" TypeName="DataAccess.LengthListDataSetTableAdapters.GetLengthByPlantTableAdapter">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="fltPlant" Name="plant_code" PropertyName="SelectedValue"
                            Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="odsProduct" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetData" TypeName="DataAccess.ProductListDataSetTableAdapters.GetALLProductTableAdapter">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="Y" Name="AddBlankEntry" Type="String" />
                        <asp:Parameter Name="BlankEntryName" Type="String" />
                        <asp:Parameter DefaultValue="All" Name="company_code" Type="String" />
                        <asp:ControlParameter ControlID="fltPlant" DefaultValue="" Name="plant_code" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:ControlParameter ControlID="fltSection" Name="section_code" PropertyName="SelectedValue"
                            Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="odsBatchCrew" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetData" TypeName="DataAccess.DummyTableAdapters.DataTable1TableAdapter">
                </asp:ObjectDataSource>
            </td>
        </tr>
        <%--        <tr>
            <td style="height: 30px">
                
            </td>
        </tr>--%>
        <tr>
            <td style="height: 100px">
                <table style="padding: 2px; background-color: #669999; width: 1130px; height: 70px;">
                    <tr>
                        <td>
                            Plant
                        </td>
                        <td style="width: 354px">
                            <asp:DropDownList ID="fltPlant" runat="server" Width="290px" AutoPostBack="True"
                                CausesValidation="True">
                            </asp:DropDownList></td>
                        <td>
                            Section
                        </td>
                        <td style="width: 367px">
                            <asp:DropDownList ID="fltSection" runat="server" Width="290px" DataSourceID="odsSection"
                                DataTextField="section_desc" DataValueField="section_code" AutoPostBack="True"
                                CausesValidation="True">
                            </asp:DropDownList></td>
                        <td>
                            Machine
                        </td>
                        <td style="width: 324px">
                            <asp:DropDownList ID="fltMachine" runat="server" Width="290px" DataSourceID="odsMachine"
                                DataTextField="machine_desc" DataValueField="machine_code" AutoPostBack="True"
                                CausesValidation="True">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td valign="top">
                            Shift
                        </td>
                        <td valign="top" style="width: 354px">
                            <asp:DropDownList ID="fltShift" runat="server" Width="290px" DataSourceID="odsShift"
                                DataTextField="shift" DataValueField="shift" AutoPostBack="True" CausesValidation="True">
                            </asp:DropDownList>
                        </td>
                        <td valign="top">
                        </td>
                        <td valign="top" style="width: 367px">
                        </td>
                        <td valign="top">
                            Date
                        </td>
                        <td valign="top" style="width: 324px">
                            <asp:TextBox ID="txtDate" runat="server" Width="80" AutoPostBack="True" CausesValidation="True"></asp:TextBox>
                            &nbsp;<asp:RegularExpressionValidator ID="rvStartDT" runat="server" ErrorMessage="dd/mm/yyyy"
                                ControlToValidate="txtDate" ValidationExpression="([1-9]|0[1-9]|[12][0-9]|3[01])[//.](0[1-9]|1[012])[//.](19|20)\d\d"
                                Display="Dynamic"></asp:RegularExpressionValidator>
                            &nbsp; &nbsp; &nbsp; &nbsp;<asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/calendar_icon.gif" />
                            <asp:Calendar ID="Calendar1" runat="server" Visible="false" Width="100"></asp:Calendar>
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <table style="width: 1130px" id="TABLE1" onclick="return TABLE1_onclick()">
                    <tr>
                        <td style="width: 400px; text-align: left;">
                            <br />
                            <asp:GridView ID="gvShiftDuration" runat="server" AutoGenerateColumns="False" DataSourceID="odsShiftDuration"
                                ShowHeader="False" BackColor="#669999" Width="100%">
                                <Columns>
                                    <asp:TemplateField InsertVisible="False" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lbShiftDurationID" runat="server" Text='<%# Bind("machine_shift_duration_id") %>'
                                                Visible="False"></asp:Label>
                                            <asp:Label ID="lbShiftDuration" runat="server" Text='<%# Eval("machine_shift_duration", "{0}") %>'
                                                Visible="False"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                                        <ItemTemplate>
                                            <asp:Label ID="lb1" runat="server" Text="Machine Shift Duration" ForeColor="#4C4C4C"></asp:Label>
                                            <asp:TextBox ID="txtShiftDuration" runat="server" Text='<%# Eval("machine_shift_duration", "{0}") %>'
                                                Width="50px" />
                                            <asp:Label ID="lb2" runat="server" Text="Minutes" ForeColor="#4C4C4C"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            &nbsp;&nbsp;<asp:Button ID="btnEdit" runat="server" Text="Update" CommandName="EditShiftDuration"
                                                CommandArgument='<%#  CType(Container,GridViewRow).RowIndex  %>' />&nbsp;&nbsp;
                                        </ItemTemplate>
                                        <ItemStyle BorderStyle="None" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                        <td style="width: 223px; text-align: left;">
                            <div style="background-color: #669999; height: 26px; position: relative; top: 8px;
                                padding: 1px;">
                                Crew
                                <asp:DropDownList ID="fltCrew" runat="server" Width="100px" DataSourceID="odsCrew"
                                    DataTextField="crew" DataValueField="crew" />
                                <asp:Button ID="btn_UpdateCrewBatch" runat="server" Text="Update" OnClientClick="if ( ConfirmMessagePopUp() != true ) return false"
                                    CommandName="UpdateCrewBatch" CommandArgument='' />
                                <%--                            <asp:GridView ID="gvMachineShiftDuration" runat="server" AutoGenerateColumns="False"
                                DataSourceID="odsMachineShiftDuration" ShowHeader="False" BackColor="#669999" Width="363px">
                                <Columns>
                                    <asp:TemplateField InsertVisible="False" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lbMachineShiftDurationID" runat="server" Text='<%# Bind("machine_shift_duration_id") %>'
                                                Visible="False"></asp:Label>
                                            <asp:Label ID="lbMachineShiftDuration" runat="server" Text='<%# Eval("machine_shift_duration", "{0}") %>'
                                                Visible="False"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Label ID="lb1" runat="server" ForeColor="#4C4C4C" Text="Machine Shift Duration"></asp:Label>
                                            <asp:TextBox ID="txtMachineShiftDuration" runat="server" Text='<%# Eval("machine_shift_duration", "{0}") %>'
                                                Width="50px" />
                                            <asp:Label ID="lb2" runat="server" ForeColor="#4C4C4C" Text="Minutes"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            &nbsp;
                                            <asp:Button ID="btnEdit" runat="server" Text="Update" CommandName="EditMachineShiftDuration"
                                                CommandArgument='<%#  CType(Container,GridViewRow).RowIndex  %>' />
                                            &nbsp;
                                        </ItemTemplate>
                                        <ItemStyle BorderStyle="None" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>--%>
                            </div>
                        </td>
                        <td>
                            <div style="background-color: #669999; height: 22px; position: relative; top: 9px;
                                padding-top: 7px;">
                                <asp:Label ID="lbTotalTimeLost" runat="server" Style="text-align: left; vertical-align: bottom;
                                    color: White;"></asp:Label>
                            </div>
                        </td>
                        <%--


                        <td style="width: 7px">
                        </td>

                        <td style="width: 200px; text-align: left;">
                            <br />
                            
                        </td>--%>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
                <asp:GridView ID="gvDowntime" runat="server" AutoGenerateColumns="False" DataSourceID="odsDowntime"
                    DataKeyNames="downtime_id" AllowPaging="True" PageSize="50" ForeColor="Black"
                    HorizontalAlign="Left" Width="900px" EnableModelValidation="True">
                    <Columns>
                        <asp:TemplateField HeaderText="Hidden" InsertVisible="False" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbDowntimeId" runat="server" Text='<%# Bind("downtime_id") %>' Visible="False"></asp:Label>
                                <asp:Label ID="lbPlantId" runat="server" Text='<%# Bind("plant_code") %>' Visible="False"></asp:Label>
                                <asp:Label ID="lbSectionId" runat="server" Text='<%# Bind("section_code") %>' Visible="False"></asp:Label>
                                <asp:Label ID="lbMachineId" runat="server" Text='<%# Bind("machine_code") %>' Visible="False"></asp:Label>
                                <asp:Label ID="lbCrew" runat="server" Text='<%# Bind("crew") %>' Visible="False"></asp:Label>
                                <asp:Label ID="lbShift" runat="server" Text='<%# Bind("shift") %>' Visible="False"></asp:Label>
                                <asp:Label ID="lbCompanyCode" runat="server" Text='<%# Bind("company_code") %>' Visible="False"></asp:Label>
                                <asp:Label ID="lbSubAreaCode" runat="server" Text='<%# Bind("sub_area_code") %>'
                                    Visible="False"></asp:Label>
                                <asp:Label ID="lbSubAreaItemCode" runat="server" Text='<%# Bind("sub_area_item_code") %>'
                                    Visible="False"></asp:Label>
                                <asp:Label ID="lbCommentRequiredFlag" runat="server" Text='<%# Bind("comment_required_flag") %>'
                                    Visible="False"></asp:Label>
                                <asp:Label ID="lbDate" runat="server" Text='<%# Bind("downtime_date") %>' Visible="False"></asp:Label>
                                <asp:Label ID="lbPlc" runat="server" Text='<%# Bind("plc_flag") %>' Visible="False"></asp:Label>
                                <asp:Label ID="lbFault" runat="server" Text='<%# Bind("fault_key") %>' Visible="False"></asp:Label>
                                <asp:Label ID="lbProblemGroup" runat="server" Text='<%# Bind("problem_group")%>' Visible="False"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Hour">
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                                <asp:DropDownList ID="dplHour" runat="server" Width="40px" DataSourceID="odsHour"
                                    DataTextField="hourName" DataValueField="hour" SelectedValue='<%# Bind("hour") %>'
                                    AutoPostBack="True" OnSelectedIndexChanged="dplHour_SelectedIndexChanged" />&nbsp;
                                <asp:ObjectDataSource ID="odsLastSort" runat="server" OldValuesParameterFormatString="original_{0}"
                                    SelectMethod="GetData" TypeName="DataAccess.LastSortDataSetTableAdapters.GetLastSortTableAdapter">
                                    <SelectParameters>
                                        <asp:ControlParameter ControlID="fltPlant" Name="plant_code" PropertyName="SelectedValue"
                                            Type="String" />
                                        <asp:ControlParameter ControlID="fltSection" Name="section_code" PropertyName="SelectedValue"
                                            Type="String" />
                                        <asp:ControlParameter ControlID="fltShift" Name="shift" PropertyName="SelectedValue"
                                            Type="String" />
                                        <asp:ControlParameter ControlID="txtDate" Name="downtime_date" PropertyName="Text"
                                            Type="DateTime" />
                                        <asp:ControlParameter ControlID="dplHour" Name="hour" PropertyName="SelectedValue"
                                            Type="Int32" />
                                    </SelectParameters>
                                </asp:ObjectDataSource>
                                <asp:ObjectDataSource ID="odsLastLength" runat="server" OldValuesParameterFormatString="original_{0}"
                                    SelectMethod="GetData" TypeName="DataAccess.LastLengthDataSetTableAdapters.GetLastLengthTableAdapter">
                                    <SelectParameters>
                                        <asp:ControlParameter ControlID="fltPlant" Name="plant_code" PropertyName="SelectedValue"
                                            Type="String" />
                                        <asp:ControlParameter ControlID="fltSection" Name="section_code" PropertyName="SelectedValue"
                                            Type="String" />
                                        <asp:ControlParameter ControlID="fltShift" Name="shift" PropertyName="SelectedValue"
                                            Type="String" />
                                        <asp:ControlParameter ControlID="txtDate" Name="downtime_date" PropertyName="Text"
                                            Type="DateTime" />
                                        <asp:ControlParameter ControlID="dplHour" Name="hour" PropertyName="SelectedValue"
                                            Type="Int32" />
                                    </SelectParameters>
                                </asp:ObjectDataSource>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Time(HH:MM:SS)">
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" Wrap="True" />
                            <ItemTemplate>
                                <asp:TextBox ID="tbHour" runat="server" Width="15px" Text='<%# Bind("start_hour") %>'></asp:TextBox><asp:RangeValidator
                                    ID="rvHour" runat="server" ErrorMessage="*" ControlToValidate="tbHour" MinimumValue="0"
                                    MaximumValue="23" Display="Dynamic"></asp:RangeValidator>:<asp:TextBox ID="tbMinute"
                                        runat="server" Width="15px" Text='<%# Bind("start_minute") %>'></asp:TextBox><asp:RangeValidator
                                            ID="rvMinute" runat="server" ErrorMessage="*" ControlToValidate="tbMinute" MinimumValue="0"
                                            MaximumValue="59" Display="Dynamic"></asp:RangeValidator>:<asp:TextBox ID="tbSecond"
                                                runat="server" Width="15px" Text='<%# Bind("start_second") %>'></asp:TextBox><asp:RangeValidator
                                                    ID="rvSecond" runat="server" ErrorMessage="*" ControlToValidate="tbSecond" MinimumValue="0"
                                                    MaximumValue="59" Display="Dynamic"></asp:RangeValidator>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Sort">
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                                <asp:DropDownList ID="dplSort" runat="server" Width="100px" DataSourceID='<%# IIF(EVAL("downtime_id") = 0,"odsSort","odsSort") %>'
                                    DataTextField="sort_desc" DataValueField="sort_code" SelectedValue='<%# Bind("sort_code") %>'
                                    Enabled='<%# IIF(EVAL("downtime_id") = 0,"True","False") %>' />&nbsp;
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Sub Area">
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                                <asp:DropDownList ID="dplSubArea" runat="server" Width="150px" DataSourceID='odsSubArea'
                                    DataTextField="sub_area_desc" DataValueField="sub_area_code" SelectedValue='<%# Bind("sub_area_code") %>'
                                    Enabled='<%# IIF (EVAL("downtime_id") = 0 or (EVAL("plc_flag") = "Y"), "True", "False") %>'
                                    AutoPostBack="True" OnSelectedIndexChanged="dplSubArea_SelectedIndexChanged"
                                    ValidationGroup="SubArea" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Problem">
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                                <asp:DropDownList ID="dplSubAreaItem" runat="server" Width="150px" DataSourceID="odsSubAreaItem"
                                    DataTextField="sub_area_item_desc" DataValueField="sub_area_item_code"
                                    AutoPostBack="true" OnSelectedIndexChanged="dplProblem_SelectedIndexChanged" />
                                <asp:ObjectDataSource ID="odsSubAreaItem" runat="server" OldValuesParameterFormatString="original_{0}"
                                    SelectMethod="GetData" TypeName="DataAccess.SubAreaItemListDataSetTableAdapters.GetSubAreaItemBySubAreaTableAdapter">
                                    <SelectParameters>
                                        <asp:ControlParameter ControlID="fltPlant" Name="plant_code" PropertyName="SelectedValue"
                                            Type="String" />
                                        <asp:ControlParameter ControlID="fltSection" Name="section_code" PropertyName="SelectedValue"
                                            Type="String" />
                                        <asp:ControlParameter ControlID="fltMachine" Name="machine_code" PropertyName="SelectedValue"
                                            Type="String" />
                                        <asp:Parameter Name="sub_area_code" Type="String" />
                                        <asp:Parameter DefaultValue="Y" Name="AddBlankEntry" Type="String" />
                                        <asp:Parameter Name="BlankEntryName" Type="String" />
                                    </SelectParameters>
                                </asp:ObjectDataSource>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Cause">
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                                <asp:DropDownList ID="dplFault" runat="server" Width="150px" DataSourceID="odsFault"
                                    DataTextField="fault" DataValueField="fault_key" 
                                    AutoPostBack="True" OnSelectedIndexChanged="dplFault_SelectedIndexChanged" />
                                <asp:ObjectDataSource ID="odsFault" runat="server" SelectMethod="GetData" TypeName="DataAccess.FaultListDataSetTableAdapters.GetALLFaultTableAdapter"
                                    OldValuesParameterFormatString="original_{0}">
                                    <SelectParameters>
                                        <asp:Parameter DefaultValue="Y" Name="AddBlankEntry" Type="String" />
                                        <asp:Parameter DefaultValue="  " Name="BlankEntryName" Type="String" />
                                        <asp:Parameter DefaultValue="All" Name="company_code" Type="String" />
                                        <asp:ControlParameter ControlID="fltPlant" DefaultValue="" Name="plant_code" PropertyName="SelectedValue"
                                            Type="String" />
                                        <asp:ControlParameter ControlID="fltSection" Name="section_code" PropertyName="SelectedValue"
                                            Type="String" />
                                        <asp:ControlParameter ControlID="fltMachine" Name="machine_code" PropertyName="SelectedValue"
                                            Type="String" />
                                        <asp:ControlParameter ControlID="dplSubArea" Name="sub_area_code" PropertyName="SelectedValue"
                                            Type="String" />
                                        <asp:Parameter DefaultValue="All" Name="sub_area_item_code" Type="String" />
                                    </SelectParameters>
                                </asp:ObjectDataSource>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Cause Group">
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                                <asp:DropDownList ID="dplCauseGroup" runat="server" DataSourceID="odsLostTime"
                                    DataTextField="lost_time_desc" DataValueField="lost_time_code" Width="80px">
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Sort/Size">
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                                <asp:DropDownList ID="dplProduct" runat="server" Width="150px" DataSourceID="odsProduct"
                                    DataTextField="product_desc" DataValueField="product_key" SelectedValue='<%# Bind("product_key") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Length">
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                                <asp:DropDownList ID="dplLength" runat="server" Width="100px" DataSourceID='<%# IIF(EVAL("downtime_id") = 0,"odsLength","odsLength") %>'
                                    DataTextField="length_desc" DataValueField="length_code" SelectedValue='<%# Bind("length_code") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Time Lost">
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                                <asp:TextBox ID="txtTimeLost" runat="server" Text='<%# Eval("time_Lost", "{0:0.###}") %>'
                                    Width="40px" />
                            </ItemTemplate>
                            <HeaderStyle Wrap="True" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Occurr">
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                                <asp:TextBox ID="txtOccurrence" runat="server" Text='<%# Eval("occurrence") %>' Width="32px" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Comment" SortExpression="Comment">
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                                <asp:TextBox ID="txtComment" runat="server" Text='<%# Eval("comment") %>' Width="240px"
                                    ValidationGroup="Comment" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Crew" Visible="False">
                            <ItemTemplate>
                                <asp:DropDownList ID="dplCrew_Update" runat="server" Width="60px" DataSourceID="odsCrew"
                                    DataTextField="crew" DataValueField="crew" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <div>
                                    <asp:Button ID="btnEdit" runat="server" Text='<%# IIF(EVAL("downtime_id") = 0,"Save","Update") %>'
                                        CommandName="EditDowntime" CommandArgument='<%# CType(Container,GridViewRow).RowIndex %>'
                                        CausesValidation="true" Width="51px" />&nbsp;
                                    <asp:Button ID="btnDelete" runat="server" Text="Delete" Visible='<%# IIF(EVAL("downtime_id") = 0,"False","True") %>'
                                        CommandName="DeleteDowntime" CommandArgument='<%# Eval("downtime_id")%>' OnClientClick="return confirm('Are you sure you want to delete?');" />
                                </div>
                            </ItemTemplate>
                            <ItemStyle BorderStyle="None" />
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle BackColor="#669999" Font-Bold="True" ForeColor="Black" />
                    <AlternatingRowStyle BackColor="#E0E0E0" />
                </asp:GridView>
                <br />
            </td>
        </tr>
        <tr>
            <td style="height: 18px">
                &nbsp;</td>
        </tr>
        <tr style="height: 100%">
            <td>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hfOriginalCrew" runat="server" Value="-1" />
</asp:Content>
