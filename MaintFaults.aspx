<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/TimberPanels.Master" Codebehind="MaintFaults.aspx.vb"
    Inherits="Downtime.MaintFaults" Title="Downtime: Maintain Faults" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
    Maintain Faults
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <table style="width: 100%;" class="content">
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:ObjectDataSource ID="odsFaults" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetData" TypeName="DataAccess.FaultsForMaintDataSetTableAdapters.GetALLFaultsForMaintTableAdapter"
                    UpdateMethod="Update" DeleteMethod="Delete">
                    <UpdateParameters>
                        <asp:Parameter Name="CompanyCode" Type="String" />
                        <asp:Parameter Name="PlantCode" Type="String" />
                        <asp:Parameter Name="SectionCode" Type="String" />
                        <asp:Parameter Name="MachineCode" Type="String" />
                        <asp:Parameter Name="SubAreaCode" Type="String" />
                        <asp:Parameter Name="SubAreaItemCode" Type="String" />
                        <asp:Parameter Name="FaultKey" Type="Int32" />
                        <asp:Parameter Name="fault" Type="String" />
                        <asp:Parameter Name="FaultCode" Type="String" />
                        <asp:Parameter Name="Sequence" Type="Int32" />
                        <asp:Parameter Name="Forgiven" Type="string" />
                        <asp:Parameter Name="CauseGroup" Type="String" />
                    </UpdateParameters>
                    <SelectParameters>
                        <asp:ControlParameter ControlID="fltCompany" Name="company_code" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:ControlParameter ControlID="fltPlant" Name="plant_code" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:ControlParameter ControlID="fltSection" Name="section_code" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:ControlParameter ControlID="fltMachine" Name="machine_code" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:ControlParameter ControlID="fltSubArea" Name="sub_area_code" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:ControlParameter ControlID="fltSubAreaItem" Name="sub_area_item_code" PropertyName="SelectedValue"
                            Type="String" />
                    </SelectParameters>
                    <DeleteParameters>
                        <asp:Parameter Name="fault_key" Type="Int32" />
                    </DeleteParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="odsPlant" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetData" TypeName="DataAccess.PlantByCompanyDataSetTableAdapters.GetALLPlantByCompanyTableAdapter">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="fltCompany" Name="company_code" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:Parameter DefaultValue="Y" Name="AddBlankEntry" Type="String" />
                        <asp:Parameter DefaultValue="All Plants" Name="BlankEntryName" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="odsSection" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetData" TypeName="DataAccess.SectionListDataSetTableAdapters.GetSectionByPlantTableAdapter">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="fltPlant" Name="plant_code" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:Parameter DefaultValue="N" Name="AddBlankEntry" Type="String" />
                        <asp:Parameter DefaultValue="" Name="BlankEntryName" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="odsMachine" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetData" TypeName="DataAccess.MachineListDataSetTableAdapters.GetMachineBySectionTableAdapter">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="fltPlant" Name="plant_code" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:ControlParameter ControlID="fltSection" Name="section_code" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:Parameter DefaultValue="Y" Name="AddBlankEntry" Type="String" />
                        <asp:Parameter Name="BlankEntryName" Type="String" DefaultValue="All Machines" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="odsCompany" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetData" TypeName="DataAccess.CompanyListDataSetTableAdapters.GetALLCompanyTableAdapter">
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="odsSubArea" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetData" TypeName="DataAccess.ValidSubAreaDataSetTableAdapters.GetValidSubAreaByMachineTableAdapter">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="fltPlant" Name="plant_code" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:ControlParameter ControlID="fltSection" Name="section_code" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:ControlParameter ControlID="fltMachine" Name="machine_code" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:Parameter DefaultValue="Y" Name="AddBlankEntry" Type="String" />
                        <asp:Parameter Name="BlankEntryName" Type="String" DefaultValue="All Sub Area" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="odsSubAreaItem" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetData" TypeName="DataAccess.ValidSubAreaItemTableAdapters.GetSubAreaItemBySubAreaTableAdapter">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="fltPlant" Name="plant_code" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:ControlParameter ControlID="fltSection" Name="section_code" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:ControlParameter ControlID="fltMachine" Name="machine_code" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:ControlParameter ControlID="fltSubArea" Name="sub_area_code" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:Parameter DefaultValue="Y" Name="AddBlankEntry" Type="String" />
                        <asp:Parameter Name="BlankEntryName" Type="String" DefaultValue="All Sub Area Item" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
        </tr>
        <tr>
            <td>
            <asp:ObjectDataSource ID="odsLostTime" runat="server" OldValuesParameterFormatString="original_{0}"
            SelectMethod="GetData" TypeName="DataAccess.LostTimeTableAdapters.GetLostTimeTableAdapter">
                <SelectParameters>
                    <asp:Parameter DefaultValue="Y" Name="AddBlankEntry" Type="String" />
                    <asp:Parameter DefaultValue="" Name="BlankEntryName" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
            </td>
        </tr>
        <tr>
            <td>
                <p>
                    <asp:Label ID="lbMessage" runat="server" CssClass="ErrorMsgBold"></asp:Label>
                </p>
            </td>
        </tr>
        <tr>
            <td>
                <table style="padding: 2px; background-color: #669999" width="500">
                    <tr>
                        <td style="width: 150px">
                            Company</td>
                        <td style="width: 400px">
                            <asp:DropDownList ID="fltCompany" runat="server" Width="350px" DataSourceID="odsCompany"
                                DataTextField="company_desc" DataValueField="company_code" AutoPostBack="True"
                                CausesValidation="True">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td style="width: 100;">
                            Plant</td>
                        <td style="width: 400;">
                            <asp:DropDownList ID="fltPlant" runat="server" Width="350px" DataSourceID="odsPlant"
                                DataTextField="plant_name" DataValueField="plant_code" AutoPostBack="True" CausesValidation="True">
                                <asp:ListItem Value="All">All Plants</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 21px">
                            Section
                        </td>
                        <td style="height: 21px">
                            <asp:DropDownList ID="fltSection" runat="server" Width="350px" DataSourceID="odsSection"
                                DataTextField="section_desc" DataValueField="section_code" AutoPostBack="True"
                                CausesValidation="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Machine
                        </td>
                        <td>
                            <asp:DropDownList ID="fltMachine" runat="server" Width="350px" DataSourceID="odsMachine"
                                DataTextField="machine_desc" DataValueField="machine_code" AutoPostBack="True"
                                CausesValidation="True">
                                <asp:ListItem Value="All">All Machines</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>Sub Area</td>
                        <td><asp:DropDownList ID="fltSubArea" runat="server" Width="350px" DataSourceID="odsSubArea"
                                DataTextField="sub_area_desc" DataValueField="sub_area_code" AutoPostBack="True"
                                CausesValidation="True">
                                <asp:ListItem Value="All">All Sub Area</asp:ListItem>
                            </asp:DropDownList></td>
                    </tr>
                     <tr id="idConditionSubAreaItem">
                        <td>Sub Area Item</td>
                        <td id="idDDLSubAreaItem"><asp:DropDownList ID="fltSubAreaItem" runat="server" Width="350px" DataSourceID="odsSubAreaItem"
                                DataTextField="sub_area_item_desc" DataValueField="sub_area_item_code" AutoPostBack="True"
                                CausesValidation="True">
                                <asp:ListItem Value="All">All Sub Area Item</asp:ListItem>
                            </asp:DropDownList></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
                <asp:GridView ID="gvFaults" runat="server" AutoGenerateColumns="False" DataSourceID="odsFaults"
                    AllowPaging="True" PageSize="50" ForeColor="Black">
                    <Columns>
                        <asp:TemplateField HeaderText="Fault Key" InsertVisible="False">
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                                <asp:Label ID="lbFaultKey" runat="server" Text='<%# Bind("fault_Key") %>' Width="50px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Company Code" InsertVisible="False">
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                                <asp:Label ID="lbCompanyCode" runat="server" Text='<%# Bind("company_code") %>' Width="50px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Plant Code" InsertVisible="False">
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                                <asp:Label ID="lbPlantCode" runat="server" Text='<%# Bind("plant_code") %>' Width="50px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Section Code" InsertVisible="False">
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                                <asp:Label ID="lbSectionCode" runat="server" Text='<%# Bind("section_code") %>' Width="50px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Machine Code" InsertVisible="False">
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                                <asp:Label ID="lbMachineCode" runat="server" Text='<%# Bind("machine_code") %>' Width="50px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Sub Area Code" InsertVisible="False">
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                                <asp:Label ID="lblSubAreaCode" runat="server" Text='<%# Bind("sub_area_code") %>' Width="50px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Sub Area Item Code">
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                                <asp:Label ID="lblSubAreaItem" runat="server" Text='<%# Bind("sub_area_item_code")%>' Width="50px" />
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Fault Code">
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                                <asp:TextBox ID="txtFaultCode" runat="server" Text='<%# Eval("fault_code") %>' Width="50px" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Fault">
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                                <asp:TextBox ID="txtFault" runat="server" Text='<%# Eval("fault") %>' Width="200px" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Sequence">
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                                <asp:TextBox ID="txtSequence" runat="server" Text='<%# Eval("sequence") %>' Width="50px" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Forgiven" >
                            <ItemStyle CssClass="dataGridContent" HorizontalAlign="Center" BorderStyle="None"  />
                            <ItemTemplate>
                             <asp:CheckBox ID="chkForgiven" runat="server" Checked='<%# Eval("forgiven") %>' ></asp:CheckBox>                            
                            </ItemTemplate>
                        </asp:TemplateField>  
                        <asp:TemplateField HeaderText="Cause Group" >
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                            <asp:DropDownList ID="ddlProblemGroup" runat="server" Width="95px" SelectedValue ='<%# Eval("cause_group")%>' DataSourceID="odsLostTime" DataTextField="lost_time_desc" 
                            DataValueField="lost_time_code" AutoPostBack="True" CausesValidation="True">
                           
                            </asp:DropDownList>                            
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button ID="btnEdit" runat="server" Text='<%# IIF(EVAL("fault_key") = "0","Save","Update") %>'
                                    CommandName="EditFault" CommandArgument='<%#  CType(Container,GridViewRow).RowIndex %>' />
                                <asp:Button ID="btnDelete" runat="server" Text='Delete' CommandName="DeleteFault"
                                    CommandArgument='<%#  CType(Container,GridViewRow).RowIndex %>' Visible='<%# IIF(EVAL("fault_key") = "0","False","True") %>'
                                    OnClientClick="return confirm('Are you sure you want to delete?');" />
                            </ItemTemplate>
                            <ItemStyle BorderStyle="None" />
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle BackColor="#669999" Font-Bold="True" ForeColor="Black" />
                    <AlternatingRowStyle BackColor="#E0E0E0" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnCancel" runat="server" Text="Back"></asp:Button>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr style="height: 100%">
            <td>
            </td>
        </tr>
    </table>
    <script type="text/javascript" language="javascript">
        window.onload = function() {
            var tr = document.getElementById("idConditionSubAreaItem");
            var ddl = document.getElementById("idDDLSubAreaItem");
            if (ddl.innerHTML == "") {
                tr.style.display = "none";
            }
            
        }
    </script>
</asp:Content>


