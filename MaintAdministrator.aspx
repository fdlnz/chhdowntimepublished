<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/TimberPanels.Master" CodeBehind="MaintAdministrator.aspx.vb" Inherits="Downtime.MaintAdministrator" 
    title="Downtime: Maintain Administrator" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
    Maintain Administrator
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
 <table style="width: 100%;" class="content">
		<tr>
			<td>
			<asp:ObjectDataSource ID="odsAdministrator" runat="server" 
                    SelectMethod="GetData" TypeName="DataAccess.AdministratorForMaintDataSetTableAdapters.GetALLAdministratorsForMaintTableAdapter" 
                    OldValuesParameterFormatString="original_{0}">
                </asp:ObjectDataSource>
              </td>
          </tr>
		  <tr>
           <td >
                <p><asp:Label ID="lbMessage" runat="server" CssClass="Msg"></asp:Label>
                </p>
            </td>
          </tr>
          <tr>
		    <td>&nbsp;
		      <asp:GridView ID="gvAdministrator" runat="server" AutoGenerateColumns="False" 
                    DataSourceID="odsAdministrator" AllowPaging="True" PageSize="50" ForeColor="Black">
                    <Columns>
                         <asp:TemplateField HeaderText="Hidden" InsertVisible="False" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbOldUserName" runat="server" Text='<%# Bind("AdministratorName") %>' Visible="False"></asp:Label>
                             </ItemTemplate>
                        </asp:TemplateField> 
                        <asp:TemplateField HeaderText="Administrator Name" >
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                            <asp:textBox ID="txtNewUserName" runat="server" Text='<%# Eval("AdministratorName") %>' Width="450px"/>                            
                            </ItemTemplate>
                        </asp:TemplateField>  
                         <asp:TemplateField>
                           <ItemStyle  BorderStyle="None" />
                            <ItemTemplate>
                                <asp:Button ID="btnEdit" runat="server" Text='<%# IIF(EVAL("AdministratorName") = "","Save","Update") %>' 
                                CommandName="EditAdministrator" CommandArgument='<%# Container.DataItemIndex %>'
                                 />&nbsp;&nbsp;
                               <asp:Button ID="btnDelete" runat="server" Text="Delete"  Visible='<%# IIF(EVAL("AdministratorName") = "","False","True") %>'
                                CommandName="DeleteAdministrator" CommandArgument='<%# Eval("AdministratorName")%>'
                                 OnClientClick="return confirm('Are you sure you want to delete?');"/>
                            </ItemTemplate>
                        </asp:TemplateField>    
                    </Columns>                   
                    <HeaderStyle BackColor="#669999" Font-Bold="True" ForeColor="Black" />
                    <AlternatingRowStyle BackColor="#E0E0E0" />
                </asp:GridView>
			    
		    </td>
		</tr>
		<tr >
			<td>
			<asp:button id="btnCancel" runat="server" Text="Back"></asp:button>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr style="height: 100%">
			<td ></td>
		</tr>
	</table>
</asp:Content>
