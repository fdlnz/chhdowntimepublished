<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/TimberPanels.Master" CodeBehind="MaintSubAreaItem.aspx.vb" Inherits="Downtime.MaintSubAreaItem" 
    title="Downtime: Maintain Sub Area Item" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
   Maintain Sub Area Item
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
 <table style="width: 100%;" class="content">
	     <tr>
			<td>
			<asp:ObjectDataSource ID="odsPlant" runat="server" OldValuesParameterFormatString="original_{0}"
             SelectMethod="GetData" TypeName="DataAccess.PlantByCompanyDataSetTableAdapters.GetALLPlantByCompanyTableAdapter">
             <SelectParameters>
                 <asp:Parameter DefaultValue="All" Name="company_code" Type="String" />
                 <asp:Parameter DefaultValue="N" Name="AddBlankEntry" Type="String" />
                 <asp:Parameter DefaultValue="" Name="BlankEntryName" Type="String" />
               </SelectParameters>
               </asp:ObjectDataSource>
  
             </td>
		 </tr>
		 <tr>
			<td>
			<asp:ObjectDataSource ID="odsSection" runat="server" 
                    SelectMethod="GetData" TypeName="DataAccess.SectionListDataSetTableAdapters.GetSectionByPlantTableAdapter" 
                    OldValuesParameterFormatString="original_{0}">
                <SelectParameters>
                    <asp:ControlParameter ControlID="fltPlant" Name="plant_code" PropertyName="SelectedValue"
                        Type="String" />                        
                    <asp:Parameter DefaultValue="N" Name="AddBlankEntry" Type="String" />
                    <asp:Parameter DefaultValue="" Name="BlankEntryName" Type="String" />
                </SelectParameters>
                </asp:ObjectDataSource>
              </td>
         </tr>
         <tr>
			<td >
			    <asp:ObjectDataSource ID="odsMachine" runat="server" 
                    OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
                    
                    TypeName="DataAccess.MachineListDataSetTableAdapters.GetMachineBySectionTableAdapter">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="fltPlant" Name="plant_code" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:ControlParameter ControlID="fltSection" Name="section_code" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:Parameter DefaultValue="N" Name="AddBlankEntry" Type="String" />
                        <asp:Parameter Name="BlankEntryName" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
              </td>
         </tr>
	     <tr>
			<td >
			<asp:ObjectDataSource ID="odsSubArea" runat="server" OldValuesParameterFormatString="original_{0}"
             SelectMethod="GetData" TypeName="DataAccess.SubAreaListDataSetTableAdapters.GetSubAreaByMachineTableAdapter">
                <SelectParameters>
                    <asp:ControlParameter ControlID="fltPlant" Name="plant_code" PropertyName="SelectedValue"
                        Type="String" />
                    <asp:ControlParameter ControlID="fltSection" Name="section_code" PropertyName="SelectedValue"
                        Type="String" />
                    <asp:ControlParameter ControlID="fltMachine" Name="machine_code" PropertyName="SelectedValue"
                        Type="String" />
                    <asp:Parameter Name="AddBlankEntry" Type="String" />
                    <asp:Parameter Name="BlankEntryName" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="odsSubAreaItem" runat="server" OldValuesParameterFormatString="original_{0}"
             SelectMethod="GetData" TypeName="DataAccess.SubAreaItemForMaintDataSetTableAdapters.GetALLSubAreaItemForMaintTableAdapter" DeleteMethod="Delete" UpdateMethod="Update" InsertMethod="Insert">
                    <DeleteParameters>
                        <asp:Parameter Name="plant_code" Type="String" />
                        <asp:Parameter Name="section_code" Type="String" />
                        <asp:Parameter Name="machine_code" Type="String" />
                        <asp:Parameter Name="sub_area_code" Type="String" />
                        <asp:Parameter Name="sub_area_item_code" Type="String" />
                        <asp:Parameter Direction="InputOutput" Name="Action" Type="String" />
                    </DeleteParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="PlantCode" Type="String" />
                        <asp:Parameter Name="SectionCode" Type="String" />
                        <asp:Parameter Name="MachineCode" Type="String" />
                        <asp:Parameter Name="SubAreaCode" Type="String" />
                        <asp:Parameter Name="SubAreaItemCode" Type="String" />
                        <asp:Parameter Name="SubAreaItemDesc" Type="String" />
                        <asp:Parameter Name="Sequence" Type="Int32" />
                        <asp:Parameter Name="RequiredFlag" Type="String" />
                        <asp:Parameter Name="OldSubAreaItemCode" Type="String" />
                        <asp:Parameter Name="PlannedFlag" Type="String" />
                        <asp:Parameter Name="Forgiven" Type="String" />
                        <asp:Parameter Name="ProblemGroup" Type="String" />
                    </UpdateParameters>
                    <SelectParameters>
                        <asp:ControlParameter ControlID="fltPlant" Name="plant_code" PropertyName="SelectedValue"
                        Type="String" />
                        <asp:ControlParameter ControlID="fltSection" Name="section_code" PropertyName="SelectedValue"
                        Type="String" />
                        <asp:ControlParameter ControlID="fltMachine" Name="machine_code" PropertyName="SelectedValue"
                        Type="String" />
                        <asp:ControlParameter ControlID="fltSubArea" Name="sub_area_code" PropertyName="SelectedValue"
                            Type="String" />
                </SelectParameters>
                    <InsertParameters>
                        <asp:Parameter Name="PlantCode" Type="String" />
                        <asp:Parameter Name="SectionCode" Type="String" />
                        <asp:Parameter Name="MachineCode" Type="String" />
                        <asp:Parameter Name="SubAreaCode" Type="String" />
                        <asp:Parameter Name="SubAreaItemCode" Type="String" />
                        <asp:Parameter Name="SubAreaItemDesc" Type="String" />
                        <asp:Parameter Name="Sequence" Type="Int32" />
                        <asp:Parameter Name="RequiredFlag" Type="String" />
                        <asp:Parameter Name="OldSubAreaItemCode" Type="String" />
                        <asp:Parameter Name="PlannedFlag" Type="String" />
                        <asp:Parameter Name="Forgiven" Type="String" />
                        <asp:Parameter Name="ProblemGroup" Type="String" />
                    </InsertParameters>
               </asp:ObjectDataSource>
             </td>
		  </tr>
            <tr>
                <td>
                    <asp:ObjectDataSource ID="odsLostTime" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetData" TypeName="DataAccess.LostTimeTableAdapters.GetLostTimeTableAdapter">
                        <SelectParameters>
                            <asp:Parameter DefaultValue="Y" Name="AddBlankEntry" Type="String" />
                            <asp:Parameter DefaultValue="" Name="BlankEntryName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
		  <tr>
		    <td >
                <p><asp:Label ID="lbMessage" runat="server" CssClass="Msg"></asp:Label>
                </p>
            </td>
          </tr>
          <tr>
            <td>
                <table style="padding: 2px; background-color: #669999" width="500">
		            <tr> 
		             <td style="width: 100;">Plant</td>
		             <td style="width: 400;"> 
			            <asp:dropdownlist id="fltPlant" runat="server" Width="350px"  
                            DataSourceID="odsPlant" DataTextField="plant_name" 
                            DataValueField="plant_code" AutoPostBack="True" CausesValidation="True"></asp:dropdownlist>
                     </td>
                  </tr>
                  <tr>
			        <td>Section </td>
			        <td>
			          <asp:dropdownlist id="fltSection" runat="server" Width="350px"  
                           DataSourceID="odsSection" DataTextField="section_desc" 
                           DataValueField="section_code" AutoPostBack="True" CausesValidation="True"></asp:dropdownlist>
                     </td>
                  </tr>		
                  <tr>
			        <td>Machine </td>
			        <td>
			          <asp:dropdownlist id="fltMachine" runat="server" Width="350px"  
                           DataSourceID="odsMachine" DataTextField="machine_desc" 
                           DataValueField="machine_code" AutoPostBack="True" CausesValidation="True"></asp:dropdownlist>
                     </td>
                  </tr>		
                    <tr>
                        <td>
                            Sub Area</td>
                        <td>
                            <asp:dropdownlist id="fltSubArea" runat="server" Width="350px"  
                           DataSourceID="odsSubArea" DataTextField="sub_area_desc" 
                           DataValueField="sub_area_code" AutoPostBack="True" CausesValidation="True">
                            </asp:dropdownlist></td>
                    </tr>
                </table>
            </td>
          </tr> 
          <tr>
		    <td>&nbsp;
		      <asp:GridView ID="gvSubAreaItem" runat="server" AutoGenerateColumns="False" 
                    DataSourceID="odsSubAreaItem" AllowPaging="True" PageSize="50" ForeColor="Black">
                    <Columns>
                        <asp:TemplateField HeaderText="Plant Code" InsertVisible="False"  >
                              <ItemStyle CssClass="dataGridContent" BorderStyle="None"  />
                            <ItemTemplate>
                                <asp:Label ID="lbPlantCode" runat="server" Text='<%# Bind("plant_code") %>' Width="50px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Section Code" InsertVisible="False"  >
                              <ItemStyle CssClass="dataGridContent" BorderStyle="None"  />
                            <ItemTemplate>
                                <asp:Label ID="lbSectionCode" runat="server" Text='<%# Bind("section_code") %>' Width="50px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Machine Code" InsertVisible="False"  >
                              <ItemStyle CssClass="dataGridContent" BorderStyle="None"  />
                            <ItemTemplate>
                                <asp:Label ID="lbMachineCode" runat="server" Text='<%# Bind("machine_code") %>' Width="50px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Sub Area Code" InsertVisible="False"  >
                              <ItemStyle CssClass="dataGridContent" BorderStyle="None"  />
                            <ItemTemplate>
                                <asp:Label ID="lbSubAreaCode" runat="server" Text='<%# Bind("sub_area_code") %>' Width="50px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Sub Area Item Code" InsertVisible="False"  >
                              <ItemStyle CssClass="dataGridContent" BorderStyle="None"  />
                            <ItemTemplate>
                                <asp:Label ID="lbSubAreaItemCode" runat="server" Text='<%# Bind("sub_area_item_code") %>' Visible="False" Width="50px"></asp:Label>                            
                                <asp:TextBox ID="txtSubAreaItemCode" runat="server" Text='<%# Bind("sub_area_item_code") %>'
                                    Visible='True' Width="50px"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Sub Area Item Desc" >
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                            <asp:textBox ID="txtSubAreaItemDesc" runat="server" Text='<%# Eval("sub_area_item_desc") %>' Width="220px"/>                            
                            </ItemTemplate>
                        </asp:TemplateField>  
                        <asp:TemplateField HeaderText="Sequence" >
                            <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                            <asp:textBox ID="txtSequence" runat="server" Text='<%# Eval("sequence") %>' Width="50px"/>                            
                            </ItemTemplate>
                        </asp:TemplateField>  
                        <asp:TemplateField HeaderText="Required" >
                            <ItemStyle CssClass="dataGridContent" HorizontalAlign="Center" BorderStyle="None" />
                            <ItemTemplate>
                             <asp:CheckBox ID="chkRequired" runat="server" Checked='<%# Eval("required_flag") %>' ></asp:CheckBox>                            
                            </ItemTemplate>
                        </asp:TemplateField>  
                         <asp:TemplateField HeaderText="Planned" >
                            <ItemStyle CssClass="dataGridContent" HorizontalAlign="Center" BorderStyle="None" />
                            <ItemTemplate>
                             <asp:CheckBox ID="chkPlannedFlag" runat="server" Checked='<%# Eval("planned_flag")%>' ></asp:CheckBox>                            
                            </ItemTemplate>
                        </asp:TemplateField>  
                         <asp:TemplateField HeaderText="Forgiven" >
                            <ItemStyle CssClass="dataGridContent" HorizontalAlign="Center" BorderStyle="None" />
                            <ItemTemplate>
                             <asp:CheckBox ID="chkForgiven" runat="server" Checked='<%# Eval("forgiven") %>' ></asp:CheckBox>                            
                            </ItemTemplate>
                        </asp:TemplateField>  
                        <asp:TemplateField HeaderText="Problem Group" >
                        <ItemStyle CssClass="dataGridContent" BorderStyle="None" />
                            <ItemTemplate>
                                <asp:DropDownList ID="ddlProblemGroup" runat="server" Width="95px" SelectedValue ='<%# Eval("problem_group") %>' DataSourceID="odsLostTime" DataTextField="lost_time_desc" 
                           DataValueField="lost_time_code" AutoPostBack="True" CausesValidation="True">
                                 <%--   <asp:ListItem Text="Electrical" Value="E" ></asp:ListItem>
                                    <asp:ListItem Text="Mechanical" Value="M" ></asp:ListItem>
                                    <asp:ListItem Text="Operational" Value="O" ></asp:ListItem>
                                    <asp:ListItem Text="Other" Value="OT" ></asp:ListItem>
                                    <asp:ListItem Text="" Value="" ></asp:ListItem>--%>
                                </asp:DropDownList>                            
                            </ItemTemplate>
                        </asp:TemplateField>  
                        <asp:TemplateField>
                           <ItemStyle BorderStyle="None"  />
                            <ItemTemplate>
                                <asp:Button ID="btnEdit" runat="server" Text='<%# IIF(EVAL("sub_area_item_code") = "","Save","Update") %>'
                                CommandName="EditSubArea" CommandArgument='<%#  CType(Container,GridViewRow).RowIndex %>' />&nbsp;&nbsp;
                                <asp:Button ID="btnDelete" runat="server" Text="Delete" Visible='<%# IIF(EVAL("sub_area_item_code") = "","False","True") %>'
                                    CommandName="DeleteSubAreaItem" CommandArgument='<%#  CType(Container,GridViewRow).RowIndex  %>'
                                    OnClientClick="return confirm('Are you sure you want to delete?');" />
                            </ItemTemplate>
                        </asp:TemplateField>    
                    </Columns>                   
                    <HeaderStyle BackColor="#669999" Font-Bold="True" ForeColor="Black" />
                    <AlternatingRowStyle BackColor="#E0E0E0" />
                </asp:GridView>
			    
		    </td>
		</tr>
		<tr >
			<td>
			<asp:button id="btnCancel" runat="server" Text="Back"></asp:button>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr style="height: 100%">
			<td ></td>
		</tr>
	</table>
</asp:Content>
