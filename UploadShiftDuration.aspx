<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/TimberPanels.Master" CodeBehind="UploadShiftDuration.aspx.vb" Inherits="Downtime.UploadShiftDuration" 
    title="Downtime: Upload Shift Duration" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
    Upload Shift Duration
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <table style="width: 100%;" class="content">    
          <tr>
            <td >
                <p><asp:Label ID="lblMessage" runat="server" CssClass="Msg"></asp:Label>
                </p>
            </td>
          </tr>
          <tr style="background-color: #669999">
               <td><asp:Label ID ="label" Text="Please select a CSV file:" Font-Bold="True" runat="server"></asp:Label></td>
          </tr>
                <tr>
                    <td valign="top">
                        <input id="fileUpload" size="70" type="file" accept=".csv" runat="server"/>
                        <input id="upload" type="submit" style="margin-left:5px" value="Upload" name="Upload" runat="server"/><br />
                        
                    </td>               
                </tr>
                <tr>
                    <td><asp:Label ID="lblInfoMessage" runat="server"  Text="The first row is the header. The required CSV file format is as follows: Plant Code, Date, Section, Shift, Minutes, Machine (blank for Panels Plant)">
                             </asp:Label></td>                    
                </tr>
            </table>

</asp:Content>
