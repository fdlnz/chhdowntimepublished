<%@ Control Language="vb" AutoEventWireup="false" Codebehind="UserMenu.ascx.vb" Inherits="Downtime.UserMenu" %>
<table id="tocwp" cellspacing="0" cellpadding="0" class="toc-layout-main">
    <tr>
        <td id="column" style="width: 100%" valign="top">
            <div class="level-section">
                <div style="margin-left: 18px; margin-right: 18px">
                    <div class="level-header">
                        <span class="headertitle">&nbsp;</span>
                    </div>
                </div>
            </div>
            <%  Dim userRole As String = CStr(Session("current_user_role"))%>
            <div class="level-section">
                <div style="margin-left: 18px; margin-right: 18px">
                    <div class="level-header">
                        <%  If userRole = "ADMIN" Or userRole = "PLANT_ADMIN" Or userRole = "USER" Then%>
                        <span class="headertitle"><a href="MaintDowntime.aspx"> Data Entry / Edit </a></span>
                        <%  Else%>
                        <span class="headertitle-band">Data Entry / Edit</span>
                        <%  End If%>
                    </div>
                </div>
            </div>
            <div class="level-section">
                <div style="margin-left: 18px; margin-right: 18px">
                    <div class="level-header">
                        <span class="headertitle"><a href="ReportAndPrint.aspx"> Report and Print Form </a></span>
                    </div>
                </div>
            </div>
            <div class="level-section">
                <div style="margin-left: 18px; margin-right: 18px">
                    <div class="level-header">
                        <%  If userRole = "ADMIN" Or userRole = "PLANT_ADMIN" Then%>
                        <span class="headertitle"><a href="MaintReference.aspx"> Maintenance </a></span>
                        <%  Else%>
                        <span class="headertitle-band">Maintenance</span>
                        <%  End If%>
                    </div>
                </div>
            </div>
        </td>
    </tr>
</table>
