﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="TimberPanelsUserMenu.ascx.vb" Inherits="Downtime.TimberPanelsUserMenu" %>
<table id="tocwp" cellspacing="0" cellpadding="0" class="toc-layout-main">
    <tr>
        <td id="column">
            <%  Dim userRole As String = CStr(Session("current_user_role"))%>

                        <%  If userRole = "ADMIN" Or userRole = "PLANT_ADMIN" Or userRole = "USER" Then%>
                        <span class="headertitle"><a href="MaintDowntime.aspx"> Data Entry / Edit </a></span>
                        <%  Else%>
                        <span class="headertitle-band">Data Entry / Edit</span>
                        <%  End If%>
                        <span class="headertitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="ReportAndPrint.aspx"> Report and Print Form </a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>

                        <%  If userRole = "ADMIN" Or userRole = "PLANT_ADMIN" Then%>
                        <span class="headertitle"><a href="MaintReference.aspx"> Maintenance </a></span>
                        <%  Else%>
                        <span class="headertitle-band">Maintenance</span>
                        <%  End If%>

        </td>
    </tr>
</table>
