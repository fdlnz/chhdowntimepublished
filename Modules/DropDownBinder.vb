﻿Friend Class DropDownBinder
    Friend Shared Sub bindDataSetToDropdownWithoutSelect(ByRef lstDropDown As DropDownList, ByVal dsDataSet As DataSet, ByVal strPreSelected As String)

        Dim objListItem As ListItem
        Dim drDataRow As DataRow
        Dim strText As String
        Dim strValue As String
        lstDropDown.Items.Clear()
        Try
            For Each drDataRow In dsDataSet.Tables(0).Rows()
                strValue = CStr(drDataRow.Item(0))
                strText = CStr(drDataRow.Item(1))
                If Not strValue Is Nothing Then
                    strValue = strValue.Trim()
                Else
                    strValue = String.Empty
                End If
                If Not strText Is Nothing Then
                    strText = strText.Trim()
                Else
                    strText = String.Empty
                End If
                objListItem = New ListItem(strText, strValue)
                If strText.Equals(strPreSelected) Then
                    objListItem.Selected = True
                End If
                lstDropDown.Items.Add(objListItem)
            Next
        Catch ex As Exception

        End Try

    End Sub
End Class
